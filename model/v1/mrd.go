package v1

import (
	"encoding/json"
	"fmt"

	canonicaljson "github.com/gibson042/canonicaljson-go"
	"github.com/hashicorp/go-multierror"
)

func UnmarshalMrd(data []byte) (MRD, error) {
	var r MRD
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *MRD) Marshal() ([]byte, error) {
	return canonicaljson.Marshal(r)
}

type MRD struct {
	ModelFiles          map[string]FileTarget `json:"modelFiles"`
	SoftwareEnvironment Environment           `json:"softwareEnvironment"`
}

// Validate checks the MRD for formal correctness and returns all found errors
func (m *MRD) Validate() error {
	var result error
	fileValues := make([]string, 0, len(m.ModelFiles))
	for _, v := range m.ModelFiles {
		if v.Path != "" {
			fileValues = append(fileValues, v.Path)
		}
	}
	if err := checkUniqueness(fileValues); err != nil {
		result = multierror.Append(result, fmt.Errorf("Invalid model file file paths: %v", err))
	}
	return result
}

type FileTarget struct {
	Path string `json:"path"`
	Hash *Hash  `json:"hash,omitempty"`
}

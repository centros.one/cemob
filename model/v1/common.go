package v1

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
)

// MtdEnvironment describes a fixed software environment used for running code
type Environment struct {
	Digest    string          `json:"digest"`
	Reference string          `json:"reference"`
	Type      EnvironmentType `json:"type"`
}

func (env Environment) String() string {
	return fmt.Sprintf("%s://%s@%s", env.Type, env.Reference, env.Digest)
}

// ImageReference returns the image reference that can be used for container engines
func (env Environment) ImageReference() string {
	return fmt.Sprintf("%s@%s", env.Reference, env.Digest)
}

// EnvironmentType describes possible types of environments
type EnvironmentType string

const (
	// Docker is the environment type that uses Docker containers
	Docker EnvironmentType = "docker"
)

// Hash represents a generic hash value for a file or directory
type Hash struct {
	Excludes      []string `json:"excludes,omitempty"`
	CombinedValue string   `json:"value"`
}

// NewHash creates a new hash with the given name and value
func NewHash(name, value string) Hash {
	return NewHashWithExcludes(name, value, nil)
}

// NewHashWithExcludes creates a new hash with the given name, value and excludes
func NewHashWithExcludes(name, value string, excludes []string) Hash {
	combinedValue := fmt.Sprintf("%s:%s", name, value)
	return Hash{
		Excludes:      excludes,
		CombinedValue: combinedValue,
	}
}

func (h *Hash) Name() string {
	slices := splitHashValue(h.CombinedValue)
	return slices[0]
}

func (h *Hash) Value() string {
	slices := splitHashValue(h.CombinedValue)
	return slices[1]
}

func splitHashValue(value string) []string {
	return strings.SplitN(value, ":", 2)
}

func validateEnvVar(env string) error {
	pattern := "^[A-Z_][A-Z0-9_]*$"
	if env != "" {
		match, err := regexp.MatchString(pattern, env)
		if err != nil {
			return err
		}
		if !match {
			return fmt.Errorf("'env' does not match pattern '%s'", pattern)
		}
	}
	return nil
}

func checkUniqueness(source []string) error {
	var sb strings.Builder
	seen := make(map[string]bool)
	duplicate := make(map[string]bool)
	sb.WriteString("Duplicated entries:")
	for _, v := range source {
		if _, ok := seen[v]; ok {
			if !duplicate[v] {
				sb.WriteString(fmt.Sprintf(" \"%s\"", v))
			}
			duplicate[v] = true
		}
		seen[v] = true
	}
	if len(duplicate) > 0 {
		return errors.New(sb.String())
	}
	return nil
}

package v1

import (
	"encoding/json"
	"errors"
	"fmt"

	canonicaljson "github.com/gibson042/canonicaljson-go"
	"github.com/hashicorp/go-multierror"
)

func UnmarshalMd(data []byte) (MD, error) {
	var r MD
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *MD) Marshal() ([]byte, error) {
	return canonicaljson.Marshal(r)
}

type MD struct {
	ImageHash Hash `json:"imageHash"`
	MRD       MRD  `json:"mrd"`
	MTD       MTD  `json:"mtd"`
}

// Validate checks the MD for formal correctness and returns all found errors
func (m *MD) Validate() error {
	var result error
	if err := m.MTD.Validate(); err != nil {
		result = multierror.Append(result, err)
	}
	if err := m.MRD.Validate(); err != nil {
		result = multierror.Append(result, err)
	}
	if err := validateHash(m.MTD.TrainingData.Hash); err != nil {
		result = multierror.Append(result, fmt.Errorf("Invalid MTD data repo: %s", err))
	}
	if err := validateHash(m.MTD.TrainingCode.Hash); err != nil {
		result = multierror.Append(result, fmt.Errorf("Invalid MTD code repo: %s", err))
	}
	for name, mf := range m.MRD.ModelFiles {
		if err := validateHash(mf.Hash); err != nil {
			result = multierror.Append(result, fmt.Errorf("Invalid MRD model file \"%s\": %s", name, err))
		}
	}
	return result
}

func validateHash(hash *Hash) error {
	if hash == nil || hash.CombinedValue == "" {
		return errors.New("Hash value missing")
	}
	return nil
}

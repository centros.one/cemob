package v1

import (
	"encoding/json"
	"fmt"

	canonicaljson "github.com/gibson042/canonicaljson-go"
	"github.com/hashicorp/go-multierror"
)

// UnmarshalMtd converts a byte array containing JSON to a MTD struct
func UnmarshalMtd(data []byte) (MTD, error) {
	var r MTD
	err := json.Unmarshal(data, &r)
	return r, err
}

// Marshal serializes the MTD to JSON bytes
func (r *MTD) Marshal() ([]byte, error) {
	return canonicaljson.Marshal(r)
}

// MTD represents a Model Training Descriptor
type MTD struct {
	Metadata            map[string]interface{} `json:"metadata"`
	ModelFiles          map[string]FileSource  `json:"modelFiles"`
	SoftwareEnvironment MtdEnvironment         `json:"softwareEnvironment"`
	TrainingCode        CodeDataSource         `json:"trainingCode"`
	TrainingData        CodeDataSource         `json:"trainingData"`
	TrainingParameters  []ValueSource          `json:"trainingParameters"`
}

// Validate checks the MTD for formal correctness and returns all found errors
func (m *MTD) Validate() error {
	var result error
	if err := checkFileSourceUniqueness(m.ModelFiles); err != nil {
		result = multierror.Append(result, fmt.Errorf("Invalid model files: %v", err))
	}
	seen := make(map[string]bool)
	duplicate := make(map[string]bool)
	for i, p := range m.TrainingParameters {
		err := p.Validate()
		if err != nil {
			result = multierror.Append(result, fmt.Errorf("trainingParameters.%d: %v", i, err))
		}
		if _, ok := seen[p.Path]; ok {
			if !duplicate[p.Path] {
				result = multierror.Append(result, fmt.Errorf("Duplicated training parameter file: %s", p.Path))
			}
			duplicate[p.Path] = true
		}
		seen[p.Path] = true
	}
	return result
}

func checkFileSourceUniqueness(source map[string]FileSource) error {
	filePaths := make([]string, len(source))
	i := 0
	for _, v := range source {
		filePaths[i] = v.FromFile
		i++
	}
	return checkUniqueness(filePaths)
}

// FileSource represents a file in the container that should be copied from it
type FileSource struct {
	FromFile string `json:"fromFile"`
}

// MtdEnvironment describes a fixed software environment used for running code
type MtdEnvironment struct {
	Environment
	ImageType ImageType `json:"imageType"`
}

// ImageType describes possible types of docker images
type ImageType string

const (
	// DvcPipeline is the image type that uses a DVC pipeline internally
	DvcPipeline ImageType = "dvc-pipeline"
)

// CodeDataSource describes a source for data or code
type CodeDataSource struct {
	Repo     string             `json:"repo"`
	Revision string             `json:"revision"`
	Type     CodeDataSourceType `json:"type"`
	Hash     *Hash              `json:"hash,omitempty"`
}

// CodeDataSourceType describes possible types for code / data sources
type CodeDataSourceType string

const (
	// Git is the source type that uses plain Git repositories
	Git CodeDataSourceType = "git"
	// DvcLocal is the source type that uses a local DVC repository
	DvcLocal CodeDataSourceType = "dvc-local"
)

// ValueSource describes a specific value that should be made available in the environment
type ValueSource struct {
	Value string `json:"value"`
	Path  string `json:"path,omitempty"`
	Env   string `json:"env,omitempty"`
}

// Validate checks the value source for formal correctness
func (v *ValueSource) Validate() error {
	// we need to check the env name again because the schema validation allows
	// FileSource entries with invalid env var names
	return validateEnvVar(v.Env)
}

FROM golang:latest AS build

WORKDIR /build
COPY . /build

ARG COMMIT_SHA
ENV COMMIT ${COMMIT_SHA}
ARG COMMIT_REF_SLUG
ENV VERSION ${COMMIT_REF_SLUG}

RUN make clean linux

FROM debian:buster-slim

RUN groupadd -g 1000 -r cemob \
    && useradd --no-log-init -r -u 1000 -g cemob cemob \
    && apt-get update && apt-get install -y \
        ca-certificates \
        curl \
        gnupg \
        openssl \
        openssh-client \
    && curl -L https://dvc.org/deb/dvc.list --output /etc/apt/sources.list.d/dvc.list \
    && (curl -Lq https://dvc.org/deb/iterative.asc | apt-key add -) \
    && apt-get update && apt-get install -y \
        dvc=2.1.0 \
    && rm -rf /var/lib/apt/lists/*


COPY --from=build /build/dist/cemob-linux-amd64 /usr/local/bin/cemob

USER cemob

WORKDIR /mnt

CMD ["cemob"]

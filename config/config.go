package config

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/knadh/koanf"
	"github.com/knadh/koanf/parsers/yaml"
	"github.com/knadh/koanf/providers/confmap"
	"github.com/knadh/koanf/providers/env"
	"github.com/knadh/koanf/providers/file"
	"gitlab.com/centros.one/cemob/hash"
)

// Config is the global configuration structure
type Config struct {
	EnvAuth       map[string]EnvAuth  `koanf:"env_auth"`
	RepoAuth      map[string]RepoAuth `koanf:"repo_auth"`
	IgnoreHostKey bool                `koanf:"ignore_host_key"`
	TrainConfig   TrainConfig         `koanf:"train"`
	WorkingDir    string              `koanf:"working_dir"`
	OutputDir     string              `koanf:"output_dir"`
	HashName      string              `koanf:"hash"`
	Hash          hash.Hash
}

// EnvAuth represents authentication information for a software environment
type EnvAuth struct {
	User string `koanf:"user"`
	Pass string `koanf:"pass"`
}

// RepoAuth represents authentication information for a code or data repository
type RepoAuth struct {
	KeyPath string `koanf:"key_path"`
	KeyPass string `koanf:"key_pass"`
	User    string `koanf:"user"`
	Pass    string `koanf:"pass"`
}

// TrainConfig represents configuration specific to the training process
type TrainConfig struct {
	CopyRepos bool `koanf:"copy_repos"`
}

// Validate validates the structural correctness of the repo auth information
func (auth *RepoAuth) Validate() error {
	if (auth.User != "" || auth.Pass != "") && auth.KeyPath != "" {
		return errors.New("Only specify one of key path or username / password")
	}
	if auth.KeyPass != "" && auth.KeyPath == "" {
		return errors.New("Key pass without key path is invalid")
	}
	return nil
}

var (
	k      = koanf.New("::")
	parser = yaml.Parser()

	configOptions = []string{
		".cemob.yaml",
		"$XDG_CONFIG_HOME/cemob/config.yaml",
		"$HOME/.config/cemob/config.yaml",
	}
	forceConfigFile = false
)
var c *Config

func SetConfigSource(filePath string) {
	if forceConfigFile {
		panic("SetConfigSource was called multiple times!")
	}
	forceConfigFile = true
	configOptions = []string{filePath}
}

// GetConfig returns the global config
// it only loads the config once and returns the cached config after that
func GetConfig() (Config, error) {
	if c == nil {
		config := Config{
			EnvAuth:  make(map[string]EnvAuth),
			RepoAuth: make(map[string]RepoAuth),
		}

		// default values
		k.Load(confmap.Provider(map[string]interface{}{
			"hash": "sha256",
		}, "."), nil)

		// read config file
		configFile := findMatchingCandidate(configOptions, !forceConfigFile)
		switch {
		case configFile != "":
			if err := k.Load(file.Provider(configFile), parser); err != nil {
				return config, fmt.Errorf("Cannot parse config file \"%s\": %v", configFile, err)
			}
		case forceConfigFile:
			return config, fmt.Errorf("Cannot open config file \"%s\"", configOptions[0])
		}

		// Load environment variables and merge into the loaded config.
		// "CEMOB__" is the prefix to filter the env vars by.
		// "." is the delimiter used to represent the key hierarchy in env vars.
		//
		// For example, env vars: CEMOB___TYPE and CEMOB___PARENT1___CHILD1_NAME___WITH__DOT
		// will be merged into the "type" and the nested "parent1::child1_name::with.dot"
		// keys in the config file here as we lowercase the key,
		// replace `___` with `::`, replace `__` with `.` and strip the CEMOB___ prefix so
		// that only "parent1::child1_name::with.dot" remains.
		if err := k.Load(env.Provider("CEMOB___", "::", func(s string) string {
			return strings.Replace(strings.Replace(strings.ToLower(
				strings.TrimPrefix(s, "CEMOB___")), "___", "::", -1), "__", ".", -1)
		}), nil); err != nil {
			return config, fmt.Errorf("Cannot parse ENV variables: %v", err)
		}

		k.Unmarshal("", &config)

		h, err := hash.NewHash(config.HashName)
		if err != nil {
			return config, err
		}
		config.Hash = h

		c = &config
	}
	return *c, nil
}

func findMatchingCandidate(candidates []string, expandEnv bool) string {
	for _, candidate := range candidates {
		if expandEnv {
			candidate = os.ExpandEnv(candidate)
		}
		info, err := os.Stat(candidate)
		if err == nil && !info.IsDir() {
			return candidate
		}
	}
	return ""
}

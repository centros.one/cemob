package statoverwrite

import (
	"os"
	"time"

	"github.com/go-git/go-billy/v5"
)

type fileInfo struct {
	os.FileInfo
	fileMode *os.FileMode
	dirMode  *os.FileMode
	linkMode *os.FileMode
	modTime  *time.Time
}

func newFileInfo(fi os.FileInfo, fileMode *os.FileMode, dirMode *os.FileMode, linkMode *os.FileMode, modTime *time.Time) fileInfo {
	return fileInfo{
		FileInfo: fi,
		fileMode: fileMode,
		dirMode:  dirMode,
		linkMode: linkMode,
		modTime:  modTime,
	}
}

func (f fileInfo) Mode() os.FileMode {
	switch {
	case f.FileInfo.IsDir() && f.dirMode != nil:
		return *f.dirMode
	case !f.FileInfo.IsDir() && f.FileInfo.Mode()&os.ModeSymlink != 0 && f.linkMode != nil:
		return *f.linkMode
	case !f.FileInfo.IsDir() && f.FileInfo.Mode()&os.ModeSymlink == 0 && f.fileMode != nil:
		return *f.fileMode
	}
	return f.FileInfo.Mode()
}

func (f fileInfo) ModTime() time.Time {
	if f.modTime != nil {
		return *f.modTime
	}
	return f.FileInfo.ModTime()
}

// StatOverwrite is a helper that allows overwriting certain fileInfo stats of the underlying fs
type StatOverwrite struct {
	billy.Filesystem
	fileMode *os.FileMode
	dirMode  *os.FileMode
	linkMode *os.FileMode
	modTime  *time.Time
}

// New creates a new filesystem wrapping up 'fs' that intercepts the calls to
// the Stat, LStat and ReadDir methods to overwrite certain attributes of the returned fileinfo
func New(fs billy.Filesystem, fileMode *os.FileMode, dirMode *os.FileMode, linkMode *os.FileMode, modTime *time.Time) billy.Filesystem {
	var fm, dm, lm *os.FileMode
	var mt *time.Time
	if fileMode != nil {
		fmCopy := *fileMode
		fm = &fmCopy
	}
	if dirMode != nil {
		dmCopy := *dirMode
		dm = &dmCopy
	}
	if linkMode != nil {
		lmCopy := *linkMode
		lm = &lmCopy
	}
	if modTime != nil {
		mtCopy := *modTime
		mt = &mtCopy
	}
	return &StatOverwrite{
		Filesystem: fs,
		fileMode:   fm,
		dirMode:    dm,
		linkMode:   lm,
		modTime:    mt,
	}
}

func (fs *StatOverwrite) Lstat(filename string) (os.FileInfo, error) {
	fi, err := fs.Filesystem.Lstat(filename)
	if err == nil {
		fi = newFileInfo(fi, fs.fileMode, fs.dirMode, fs.linkMode, fs.modTime)
	}
	return fi, err
}

func (fs *StatOverwrite) Stat(filename string) (os.FileInfo, error) {
	fi, err := fs.Filesystem.Stat(filename)
	if err == nil {
		fi = newFileInfo(fi, fs.fileMode, fs.dirMode, fs.linkMode, fs.modTime)
	}
	return fi, err
}

func (fs *StatOverwrite) ReadDir(path string) ([]os.FileInfo, error) {
	fis, err := fs.Filesystem.ReadDir(path)
	if err == nil {
		newFis := make([]os.FileInfo, len(fis))
		for i, fi := range fis {
			newFis[i] = newFileInfo(fi, fs.fileMode, fs.dirMode, fs.linkMode, fs.modTime)
		}
		fis = newFis
	}
	return fis, err
}

package statoverwrite

import (
	"os"
	"testing"
	"time"

	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-billy/v5/memfs"
)

var baseFileMode os.FileMode = 0156
var baseDirMode os.FileMode = 0651 | os.ModeDir
var baseLinkMode os.FileMode = 0777 | os.ModeSymlink
var filename string = "a"
var dirname string = "b"
var linkname string = "d"

func TestFileModeOverwrite(t *testing.T) {
	var overwriteFileMode os.FileMode = 0555
	fs, err := createTestFileSystem(baseFileMode, baseDirMode)
	if err != nil {
		t.Fatalf("Could not create test fs: %v", err)
	}
	overlayFs := New(fs, &overwriteFileMode, nil, nil, nil)
	checkFileMode(t, fs, "original", filename, baseFileMode)
	checkFileMode(t, overlayFs, "overlay", filename, overwriteFileMode)
	checkDirMode(t, overlayFs, "overlay", dirname, baseDirMode)
	checkLinkMode(t, overlayFs, "overlay", linkname, baseLinkMode)
}

func TestDirModeOverwrite(t *testing.T) {
	var overwriteDirMode os.FileMode = 0555 | os.ModeDir
	fs, err := createTestFileSystem(baseFileMode, baseDirMode)
	if err != nil {
		t.Fatalf("Could not create test fs: %v", err)
	}
	overlayFs := New(fs, nil, &overwriteDirMode, nil, nil)
	checkDirMode(t, fs, "original", dirname, baseDirMode)
	checkDirMode(t, overlayFs, "overlay", dirname, overwriteDirMode)
	checkFileMode(t, overlayFs, "overlay", filename, baseFileMode)
	checkLinkMode(t, overlayFs, "overlay", linkname, baseLinkMode)
}

func TestModTimeOverwrite(t *testing.T) {
	var overwriteModTime time.Time = time.Now()
	fs, err := createTestFileSystem(0666, 0777)
	if err != nil {
		t.Fatalf("Could not create test fs: %v", err)
	}
	overlayFs := New(fs, nil, nil, nil, &overwriteModTime)
	stat, err := fs.Stat(dirname)
	if err != nil {
		t.Fatalf("Could not stat test dir in original fs: %v", err)
	}
	if !stat.IsDir() {
		t.Fatalf("Created directory is not a directory in original fs")
	}
	if stat.ModTime() == overwriteModTime {
		t.Errorf("Mod time in original fs was same as overwrite mod time, got: %s.", overwriteModTime)
	}
	stat, err = overlayFs.Stat(dirname)
	if err != nil {
		t.Fatalf("Could not stat test dir in overlay fs: %v", err)
	}
	if !stat.IsDir() {
		t.Fatalf("Created directory is not a directory in overlay fs")
	}
	if stat.ModTime() != overwriteModTime {
		t.Errorf("Mod time in overlay fs was incorrect, got: %s, want: %s.", stat.ModTime(), overwriteModTime)
	}
}

func TestFileModeOverwriteInDirs(t *testing.T) {
	var overwriteFileMode os.FileMode = 0555
	fs, err := createTestFileSystem(baseFileMode, baseDirMode)
	if err != nil {
		t.Fatalf("Could not create test fs: %v", err)
	}
	overlayFs := New(fs, &overwriteFileMode, nil, nil, nil)
	files, err := fs.ReadDir(dirname)
	if err != nil {
		t.Fatalf("Could not list dir in original fs: %v", err)
	}
	for _, stat := range files {
		if stat.Mode() != baseFileMode {
			t.Errorf("File mode in original fs was incorrect, got: %s, want: %s.", stat.Mode(), baseFileMode)
		}
	}
	files, err = overlayFs.ReadDir(dirname)
	if err != nil {
		t.Fatalf("Could not list dir in overlay fs: %v", err)
	}
	for _, stat := range files {
		if stat.Mode() != overwriteFileMode {
			t.Errorf("File mode in overlay fs was incorrect, got: %s, want: %s.", stat.Mode(), overwriteFileMode)
		}
	}
	checkDirMode(t, overlayFs, "overlay", dirname, baseDirMode)
	checkFileMode(t, overlayFs, "overlay", filename, overwriteFileMode)
	checkLinkMode(t, overlayFs, "overlay", linkname, baseLinkMode)
}

func TestFileModeOverwriteForSymlink(t *testing.T) {
	var overwriteLinkMode os.FileMode = 0555 | os.ModeSymlink
	fs, err := createTestFileSystem(0666, 0777)
	if err != nil {
		t.Fatalf("Could not create test fs: %v", err)
	}
	overlayFs := New(fs, nil, nil, &overwriteLinkMode, nil)
	checkLinkMode(t, fs, "original", linkname, baseLinkMode)
	checkLinkMode(t, overlayFs, "overlay", linkname, overwriteLinkMode)
}

func createTestFileSystem(originalFileMode os.FileMode, originalDirMode os.FileMode) (billy.Filesystem, error) {
	fs := memfs.New()
	err := createFile(fs, filename, originalFileMode)
	if err != nil {
		return nil, err
	}
	err = fs.MkdirAll(dirname, originalDirMode)
	if err != nil {
		return nil, err
	}
	err = createFile(fs, fs.Join(dirname, "c"), originalFileMode)
	if err != nil {
		return nil, err
	}
	err = fs.Symlink(filename, linkname)
	if err != nil {
		return nil, err
	}
	return fs, err
}

func createFile(fs billy.Filesystem, filename string, originalFileMode os.FileMode) error {
	f, err := fs.OpenFile("a", os.O_RDWR|os.O_CREATE, originalFileMode)
	if err != nil {
		return err
	}
	if err := f.Close(); err != nil {
		return err
	}
	return nil
}

func checkFileMode(t *testing.T, fs billy.Filesystem, fsName string, filename string, expectedFileMode os.FileMode) {
	stat, err := fs.Stat(filename)
	if err != nil {
		t.Fatalf("Could not stat file \"%s\" in %s fs: %v", filename, fsName, err)
	}
	if stat.Mode() != expectedFileMode {
		t.Errorf("File mode in %s fs for \"%s\" was incorrect, got: %s, want: %s.", fsName, filename, stat.Mode(), expectedFileMode)
	}
}

func checkDirMode(t *testing.T, fs billy.Filesystem, fsName string, dirname string, expectedDirMode os.FileMode) {
	stat, err := fs.Stat(dirname)
	if err != nil {
		t.Fatalf("Could not stat dir \"%s\" in %s fs: %v", dirname, fsName, err)
	}
	if !stat.IsDir() {
		t.Fatalf("Created directory is not a directory in %s fs", fsName)
	}
	if stat.Mode() != expectedDirMode {
		t.Errorf("Dir mode in %s fs for \"%s\" was incorrect, got: %s, want: %s.", fsName, dirname, stat.Mode(), expectedDirMode)
	}
}

func checkLinkMode(t *testing.T, fs billy.Filesystem, fsName string, filename string, expectedFileMode os.FileMode) {
	stat, err := fs.Lstat(filename)
	if err != nil {
		t.Fatalf("Could not stat symlink \"%s\" in %s fs: %v", filename, fsName, err)
	}
	if stat.Mode() != expectedFileMode {
		t.Errorf("Link mode in %s fs for \"%s\" was incorrect, got: %s, want: %s.", fsName, filename, stat.Mode(), expectedFileMode)
	}
}

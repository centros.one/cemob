package vcs

import (
	"errors"
	"fmt"
	"os/exec"
	"strings"
)

// DvcRepo represents a DVC repository
type DvcRepo struct {
	GitRepo
}

// NewDvcRepo creates a DVC repo instance with default auth
// localPath is the file system working directory
func NewDvcRepo(localPath string) *DvcRepo {
	gitRepo := NewGitRepo(localPath)
	return &DvcRepo{
		*gitRepo,
	}
}

// NewDvcRepoWithPublicKeyAuth creates a DVC repo instance with public key auth
// localPath is the file system working directory
// keyPath is the path to a file containing the PEM private key
// keyPass the password to decrypt the PEM private key (leave empty if not encrypted)
func NewDvcRepoWithPublicKeyAuth(localPath, keyPath, keyPass string) *DvcRepo {
	gitRepo := NewGitRepoWithPublicKeyAuth(localPath, keyPath, keyPass)
	return &DvcRepo{
		*gitRepo,
	}
}

// NewDvcRepoWithUserPass creates a DVC repo instance with http basic auth
// localPath is the file system working directory
// user is the username for basic auth
// pass is the password for basic auth
func NewDvcRepoWithUserPass(localPath, user, pass string) *DvcRepo {
	gitRepo := NewGitRepoWithUserPass(localPath, user, pass)
	return &DvcRepo{
		*gitRepo,
	}
}

// Initialize does the initial checkout of a remote DVC repo
// reference is the Git remote URL
func (repo *DvcRepo) Initialize(reference string) error {
	if _, err := exec.LookPath("dvc"); err != nil {
		return errors.New("Could not find \"dvc\" command")
	}
	return repo.GitRepo.Initialize(reference)
}

// Checkout makes sure the local working tree corresponds to the given revision
// revision is the revision to check out
func (repo *DvcRepo) Checkout(revision string) error {
	err := repo.GitRepo.Checkout(revision)
	if err != nil {
		return err
	}
	pullCmd := exec.Command("dvc", "pull")
	pullCmd.Dir = repo.localPath
	// save the output in case of error to print a useful message to the user
	stdoutStderr, err := pullCmd.CombinedOutput()
	if err != nil {
		if len(stdoutStderr) > 0 {
			return fmt.Errorf("Error executing command \"%s\": %s", strings.Join(pullCmd.Args, " "), stdoutStderr)
		}
		return err
	}
	return nil
}

// GetExcludeList returns a list of patterns that should be excluded when calculating
// the repo hash
func (repo *DvcRepo) GetExcludeList() []string {
	gitExcludes := repo.GitRepo.GetExcludeList()
	return append(gitExcludes, "/.dvc")
}

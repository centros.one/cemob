package vcs

import (
	"errors"

	git "github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/transport"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/go-git/go-git/v5/plumbing/transport/ssh"
	"gitlab.com/centros.one/cemob/config"
	gossh "golang.org/x/crypto/ssh"
)

func init() {
	// we overwrite the default auth builder to create a auth method that
	// respects our ignore_host_key setting
	ssh.DefaultAuthBuilder = func(user string) (ssh.AuthMethod, error) {
		authMethod, err := ssh.NewSSHAgentAuth(user)
		if err == nil {
			setHostCallback(&authMethod.HostKeyCallbackHelper)
		}
		return authMethod, err
	}
}

func setHostCallback(hostKey *ssh.HostKeyCallbackHelper) error {
	config, err := config.GetConfig()
	if err != nil {
		return err
	}
	if config.IgnoreHostKey {
		hostKey.HostKeyCallback = gossh.InsecureIgnoreHostKey()
	}
	return nil
}

// GitRepo represents a Git repository
type GitRepo struct {
	localPath string
	auth      AuthMethod
	repo      *git.Repository
}

// AuthMethod is the interface for different authentication methods
type AuthMethod interface {
	GetAuth() (transport.AuthMethod, error)
}

// UserPassAuth does http(s) basic auth using username and password
type UserPassAuth struct {
	user string
	pass string
}

// GetAuth creates the git-go AuthMethod
func (auth *UserPassAuth) GetAuth() (transport.AuthMethod, error) {
	if auth.user == "" || auth.pass == "" {
		return nil, errors.New("User or password is empty")
	}
	return &http.BasicAuth{
		Username: auth.user,
		Password: auth.pass,
	}, nil
}

// PublicKeyAuth does SSH auth using a specific private key
type PublicKeyAuth struct {
	keyPath string
	keyPass string
}

// GetAuth creates the git-go AuthMethod
func (auth *PublicKeyAuth) GetAuth() (transport.AuthMethod, error) {
	if auth.keyPath == "" {
		return nil, errors.New("Key path is empty")
	}
	publicKeyAuth, err := ssh.NewPublicKeysFromFile("git", auth.keyPath, auth.keyPass)
	if err == nil {
		setHostCallback(&publicKeyAuth.HostKeyCallbackHelper)
	}
	return publicKeyAuth, err
}

// NewGitRepo creates a Git repo instance with default auth
// localPath is the file system working directory
func NewGitRepo(localPath string) *GitRepo {
	return &GitRepo{
		localPath: localPath,
	}
}

// NewGitRepoWithPublicKeyAuth creates a Git repo instance with public key auth
// localPath is the file system working directory
// keyPath is the path to a file containing the PEM private key
// keyPass the password to decrypt the PEM private key (leave empty if not encrypted)
func NewGitRepoWithPublicKeyAuth(localPath, keyPath, keyPass string) *GitRepo {
	auth := PublicKeyAuth{
		keyPath: keyPath,
		keyPass: keyPass,
	}
	return &GitRepo{
		localPath: localPath,
		auth:      &auth,
	}
}

// NewGitRepoWithUserPass creates a Git repo instance with http basic auth
// localPath is the file system working directory
// user is the username for basic auth
// pass is the password for basic auth
func NewGitRepoWithUserPass(localPath, user, pass string) *GitRepo {
	auth := UserPassAuth{
		user: user,
		pass: pass,
	}
	return &GitRepo{
		localPath: localPath,
		auth:      &auth,
	}
}

// Initialize does the initial checkout of a remote Git repo
// reference is the Git remote URL
func (repo *GitRepo) Initialize(reference string) error {
	if repo.localPath == "" {
		return errors.New("Repo is not initialized")
	}
	if repo.repo != nil {
		return errors.New("This repo was alread initialized")
	}
	var auth transport.AuthMethod = nil
	var err error
	if repo.auth != nil {
		auth, err = repo.auth.GetAuth()
		if err != nil {
			return err
		}
	}
	rep, err := git.PlainClone(repo.localPath, false, &git.CloneOptions{
		URL:               reference,
		RecurseSubmodules: git.DefaultSubmoduleRecursionDepth,
		Auth:              auth,
	})
	if err != nil {
		return err
	}
	repo.repo = rep
	return nil
}

// Checkout makes sure the local working tree corresponds to the given revision
// revision is the revision to check out
func (repo *GitRepo) Checkout(revision string) error {
	if repo.repo == nil {
		return errors.New("Repo is not initialized")
	}
	hash, err := repo.repo.ResolveRevision(plumbing.Revision(revision))
	if err != nil {
		return err
	}
	worktree, err := repo.repo.Worktree()
	if err != nil {
		return err
	}
	err = worktree.Checkout(&git.CheckoutOptions{
		Hash:  *hash,
		Force: true,
	})
	if err != nil {
		return err
	}
	submodules, err := worktree.Submodules()
	if err != nil {
		return err
	}
	err = submodules.Update(&git.SubmoduleUpdateOptions{})
	return err
}

// LocalPath returns the local path of the working directory
func (repo *GitRepo) LocalPath() (string, error) {
	if repo.localPath == "" {
		return "", errors.New("Repo is not initialized")
	}
	return repo.localPath, nil
}

// GetExcludeList returns a list of patterns that should be excluded when calculating
// the repo hash
func (repo *GitRepo) GetExcludeList() []string {
	return []string{
		"/.git",
	}
}

package vcs

// Repo describes a VCS implementation
type Repo interface {
	Initialize(reference string) error
	Checkout(revision string) error
	LocalPath() (string, error)
	GetExcludeList() []string
}

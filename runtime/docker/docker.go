package docker

import (
	"archive/tar"
	"bufio"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"path/filepath"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/stdcopy"
	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-billy/v5/memfs"
	"gitlab.com/centros.one/cemob/hash"
	v1 "gitlab.com/centros.one/cemob/model/v1"
	"gitlab.com/centros.one/cemob/runtime/docker/model"
	"gitlab.com/centros.one/cemob/util"
)

// Docker represents a docker client with auth info
type Docker struct {
	docker *client.Client
	auth   map[string]types.AuthConfig
}

// NewDocker creates a new Docker client
func NewDocker() (*Docker, error) {
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, err
	}
	return &Docker{
		docker: cli,
		auth:   make(map[string]types.AuthConfig),
	}, nil
}

// Close closes the active transport
func (docker *Docker) Close() error {
	return docker.docker.Close()
}

// AddAuth adds authentication to the internal store of the client
// registry is the hostname of the registry to store credentials for
// username is the username to use for auth
// password is the password to use for auth
func (docker *Docker) AddAuth(registry, username, password string) {
	authConfig := types.AuthConfig{
		Username: username,
		Password: password,
	}
	docker.auth[registry] = authConfig
}

// GetEncodedAuth returns base64 encoded authentication for the given registry if in local store
// registry is the hostname of the registry to store credentials for
func (docker *Docker) GetEncodedAuth(registry string) (string, error) {
	var auth string
	if authConfig, ok := docker.auth[registry]; ok {
		encodedJSON, err := json.Marshal(authConfig)
		if err != nil {
			return "", fmt.Errorf("error when encoding authConfig. err: %v", err)
		}
		auth = base64.StdEncoding.EncodeToString(encodedJSON)
	}
	return auth, nil
}

// CreateContainer creates a container with the given options
// It pulls the image beforehand and returns the id of the created container
// ctx is the context to be used
// containerConfig is the container config to be used
// hostConfig is the host config to be used
func (docker *Docker) CreateContainer(ctx context.Context, containerConfig container.Config, hostConfig container.HostConfig) (string, error) {
	host := strings.SplitN(containerConfig.Image, "/", 2)[0]
	// will be empty if there is no auth for this registry or there is an error getting the auth data
	auth, _ := docker.GetEncodedAuth(host)

	reader, err := docker.docker.ImagePull(ctx, containerConfig.Image, types.ImagePullOptions{
		RegistryAuth: auth,
	})
	if err != nil {
		return "", fmt.Errorf("Pull of image \"%s\" failed: %v", containerConfig.Image, err)
	}
	defer reader.Close()
	// we need to wait until the reader is done to make sure that the pull was actually completed
	io.Copy(io.Discard, reader)

	resp, err := docker.docker.ContainerCreate(ctx, &containerConfig, &hostConfig, nil, nil, "")
	if err != nil {
		return "", fmt.Errorf("Could not create Docker container: %v", err)
	}
	return resp.ID, nil
}

// RunContainer starts a container with the given id
// It returns an error if the container did not exit with a zero exit code
// ctx is the context to be used
// containerID is the id the the container to be started
func (docker *Docker) RunContainer(ctx context.Context, containerID string) (<-chan string, <-chan error) {
	outc := make(chan string)
	errc := make(chan error, 2)

	go func() {
		defer close(outc)
		defer close(errc)

		containerInfo, err := docker.docker.ContainerInspect(ctx, containerID)
		if err != nil {
			errc <- fmt.Errorf("Could not retrieve config for container \"%s\": %v", containerID, err)
			return
		}

		if err := docker.docker.ContainerStart(ctx, containerID, types.ContainerStartOptions{}); err != nil {
			errc <- fmt.Errorf("Could not start Docker container \"%s\": %v", containerID, err)
			return
		}

		doneCh := make(chan struct{})
		go func() {
			defer close(doneCh)

			reader, err := docker.docker.ContainerLogs(ctx, containerID, types.ContainerLogsOptions{
				ShowStdout: true,
				ShowStderr: true,
				Follow:     true,
				Timestamps: false,
			})
			if err != nil {
				errc <- err
				return
			}
			defer reader.Close()

			if !containerInfo.Config.Tty {
				channelWriter := util.NewChannelWriter(outc)
				defer channelWriter.Close()
				stdcopy.StdCopy(channelWriter, channelWriter, reader)
			} else {
				scanner := bufio.NewScanner(reader)
				for scanner.Scan() {
					outc <- scanner.Text()
				}
			}
		}()

		statusCh, errCh := docker.docker.ContainerWait(ctx, containerID, container.WaitConditionNotRunning)
		select {
		case err := <-errCh:
			if err != nil {
				errc <- fmt.Errorf("Could not wait for Docker container \"%s\" to finish: %v", containerID, err)
			}
		case status := <-statusCh:
			switch {
			case status.Error != nil:
				errc <- fmt.Errorf("Error when running container \"%s\": %v", containerID, errors.New(status.Error.Message))
			case status.StatusCode != 0:
				errc <- fmt.Errorf("Non-zero exit code for container \"%s\": %d", containerID, status.StatusCode)
			}
		}

		// wait until the log reader is done so we can close the remaining channels
		<-doneCh
	}()

	return outc, errc
}

// CopyFromContainer is a wrapper method for Client.CopyFromContainer
func (docker *Docker) CopyFromContainer(ctx context.Context, containerID, srcPath string) (*util.TarReaderCloser, error) {
	reader, _, err := docker.docker.CopyFromContainer(ctx, containerID, srcPath)
	tarReader := util.NewTarReaderCloser(reader)
	return tarReader, err
}

// CopyToContainer is a wrapper method for Client.CopyToContainer
func (docker *Docker) CopyToContainer(ctx context.Context, containerID, dstPath string, content io.Reader, options types.CopyToContainerOptions) error {
	return docker.docker.CopyToContainer(ctx, containerID, dstPath, content, options)
}

// ContainerRemove is a wrapper method for Client.ContainerRemove
func (docker *Docker) ContainerRemove(ctx context.Context, containerID string, options types.ContainerRemoveOptions) error {
	return docker.docker.ContainerRemove(ctx, containerID, options)
}

// ImageSave is a wrapper method for Client.ImageSave
func (docker *Docker) ImageSave(ctx context.Context, imageIDs []string) (*util.TarReaderCloser, error) {
	reader, err := docker.docker.ImageSave(ctx, imageIDs)
	tarReader := util.NewTarReaderCloser(reader)
	return tarReader, err
}

// ImageBuild builds an image with the given options
// ctx is the context to be used
// buildContext is the TarReader representing the build context to be sent
// options are the image build options to be used
func (docker *Docker) ImageBuild(ctx context.Context, buildContext io.Reader, options types.ImageBuildOptions) (<-chan string, <-chan string, <-chan error) {
	logc := make(chan string)
	idc := make(chan string, 1)
	errc := make(chan error, 1)
	options.AuthConfigs = docker.auth

	go func() {
		defer close(logc)
		defer close(idc)
		defer close(errc)

		imageBuild, err := docker.docker.ImageBuild(ctx, buildContext, options)
		if err != nil {
			errc <- err
			return
		}

		defer imageBuild.Body.Close()

		scanner := bufio.NewScanner(imageBuild.Body)
		for scanner.Scan() {
			var log buildLogMessage
			line := scanner.Text()
			if err := json.Unmarshal([]byte(line), &log); err != nil {
				errc <- fmt.Errorf("Cannot parse daemon log line: %s", line)
				return
			}
			if log.Error.Error != "" {
				errc <- errors.New(log.Error.Error)
				return
			}
			if log.Error.Message != "" {
				errc <- errors.New(log.Error.Message)
				return
			}
			if log.Aux.Id != "" {
				select {
				case idc <- log.Aux.Id:
				default:
					// channel is full because we already sent an id, in that case do nothing
				}
				continue
			}
			// TODO: handle progress events
			// In case of progress events (e.g. downloading a base image) we get log messages
			// without a Stream value. We just ignore those for now.
			if log.Stream != "" {
				logc <- log.Stream
			}
		}
	}()

	return logc, idc, errc
}

type buildLogMessage struct {
	Stream string `json:"stream"`
	Aux    struct {
		Id string `json:"ID"`
	} `json:"aux"`
	Error ErrorMessage `json:"errorDetail"`
}

// ImagePush is a wrapper method for Client.ImagePush
func (docker *Docker) ImagePush(ctx context.Context, imageTag string, options types.ImagePushOptions) (<-chan ProgressMessage, <-chan error) {
	outc := make(chan ProgressMessage)
	errc := make(chan error, 1)
	host := strings.SplitN(imageTag, "/", 2)[0]
	// will be empty if there is no auth for this registry or there is an error getting the auth data
	auth, _ := docker.GetEncodedAuth(host)
	options.RegistryAuth = auth
	go func() {
		defer close(outc)
		defer close(errc)

		imagePush, err := docker.docker.ImagePush(ctx, imageTag, options)
		if err != nil {
			errc <- err
			return
		}
		defer imagePush.Close()

		scanner := bufio.NewScanner(imagePush)
		for scanner.Scan() {
			var progress ProgressMessage
			line := scanner.Text()
			if err := json.Unmarshal([]byte(line), &progress); err != nil {
				errc <- fmt.Errorf("Cannot parse progress message line: %s", line)
				return
			}
			if progress.Error.Error != "" {
				errc <- errors.New(progress.Error.Error)
				return
			}
			if progress.Error.Message != "" {
				errc <- errors.New(progress.Error.Message)
				return
			}
			outc <- progress
		}
	}()
	return outc, errc
}

type ProgressMessage struct {
	Status         string `json:"status"`
	Id             string `json:"id"`
	ProgressDetail struct {
		Current *int64 `json:"current,omitempty"`
		Total   *int64 `json:"total,omitempty"`
	} `json:"progressDetail"`
	Error ErrorMessage `json:"errorDetail"`
}

type ErrorMessage struct {
	Message string `json:"message"`
	Error   string `json:"error"`
}

// GetImageHash calculates a stable image hash for the image of the given id using the given hasher
// It will request the image from the Docker daemon and fail if it does not exist
func (docker *Docker) GetImageHash(ctx context.Context, imageID string, hasher hash.Hash) (v1.Hash, error) {
	var result v1.Hash
	tarReader, err := docker.ImageSave(ctx, []string{imageID})
	if err != nil {
		return result, err
	}
	defer tarReader.Close()
	return getImageHashFromReader(&tarReader.Reader, imageID, hasher)
}

func getImageHashFromReader(tarReader *tar.Reader, imageID string, hasher hash.Hash) (v1.Hash, error) {
	var result v1.Hash
	configName := fmt.Sprintf("%s.json", strings.Replace(imageID, "sha256:", "", 1))
	var manifest model.Manifest
	var imageConfig model.ImageConfig
	layerFilesystems := make(map[string]billy.Filesystem)
	for {
		hdr, err := tarReader.Next()
		if err == io.EOF {
			break // End of archive
		}
		if err != nil {
			return result, fmt.Errorf("Could not read next entry from tar: %v", err)
		}
		if hdr.Typeflag == tar.TypeReg {
			name := hdr.Name
			switch {
			case name == "manifest.json":
				manifest, err = model.UnmarshalManifest(tarReader)
			case name == configName:
				imageConfig, err = model.UnmarshalImageConfig(tarReader)
			case strings.HasSuffix(name, ".tar"):
				layerReader := tar.NewReader(tarReader)
				fs := memfs.New()
				h := hasher.New()
				for {
					hdr, err := layerReader.Next()
					if err == io.EOF {
						break // End of archive
					}
					if err != nil {
						return result, fmt.Errorf("Could not read next entry from layer \"%s\" tar: %v", name, err)
					}
					switch hdr.Typeflag {
					case tar.TypeDir:
						err := fs.MkdirAll(hdr.Name, 0777)
						if err != nil {
							return result, fmt.Errorf("Could not create dir \"%s\" in layer fs: %v", hdr.Name, err)
						}
					case tar.TypeReg:
						h.Reset()
						dir := filepath.Dir(hdr.Name)
						if err := fs.MkdirAll(dir, 0777); err != nil {
							return result, fmt.Errorf("Could not create dir \"%s\" in layer fs: %v", dir, err)
						}
						if _, err := io.Copy(h, layerReader); err != nil {
							return result, fmt.Errorf("Could not write \"%s\" to hasher: %v", hdr.Name, err)
						}
						f, err := fs.Create(hdr.Name)
						if err != nil {
							return result, fmt.Errorf("Could not create file \"%s\" in layer fs: %v", hdr.Name, err)
						}
						if _, err := io.WriteString(f, hasher.GetHash(h)); err != nil {
							f.Close()
							return result, fmt.Errorf("Could not write hash to file \"%s\" in layer fs: %v", hdr.Name, err)
						}
						if err := f.Close(); err != nil {
							return result, fmt.Errorf("Could not close file \"%s\" in layer fs: %v", hdr.Name, err)
						}
					case tar.TypeLink, tar.TypeSymlink:
						err = fs.Symlink(hdr.Linkname, hdr.Name)
					}
				}
				layerFilesystems[name] = fs
			}
			if err != nil {
				return result, fmt.Errorf("Err handling file \"%s\" in layer fs: %v", name, err)
			}
		}
	}
	if len(manifest) != 1 {
		return result, fmt.Errorf("Expected 1 manifest entry, got %d", len(manifest))
	}
	manifestEntry := manifest[0]
	layerCount := len(manifestEntry.Layers)
	if layerCount == 0 {
		return result, errors.New("Got no rootfs layers in manifest")
	}
	diffCount := len(imageConfig.RootFs.DiffIds)
	if layerCount != diffCount {
		return result, fmt.Errorf("Got %d layers in manifest but %d diff ids in image config", layerCount, diffCount)
	}
	passthrough := hash.NewPassthroughHash(hasher.New().Size())
	for i, layer := range manifestEntry.Layers {
		fs, ok := layerFilesystems[layer]
		if !ok {
			return result, fmt.Errorf("Could not find file system layer %s", layer)
		}
		hash, err := hash.Dirhash(fs, "", passthrough, hasher, hasher)
		if err != nil {
			return result, err
		}
		imageConfig.RootFs.DiffIds[i] = fmt.Sprintf("%s:%s", hasher.Name(), hash)
	}
	configJson, err := imageConfig.Marshal()
	if err != nil {
		return result, fmt.Errorf("Could not marshal image config: %v", err)
	}
	h := hasher.New()
	if _, err := h.Write(configJson); err != nil {
		return result, fmt.Errorf("Could not hash image config: %v", err)
	}
	hash := hasher.GetHash(h)
	result = v1.NewHash(hasher.Name(), hash)
	return result, nil
}

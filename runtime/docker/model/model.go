package model

import (
	"encoding/json"
	"io"

	"github.com/gibson042/canonicaljson-go"
)

// Manifest represents the manifest.json of a Docker image tar archive
type Manifest []ManifestEntry

func UnmarshalManifest(reader io.Reader) (Manifest, error) {
	var r Manifest
	decoder := json.NewDecoder(reader)
	err := decoder.Decode(&r)
	return r, err
}

func (r *Manifest) Marshal() ([]byte, error) {
	return canonicaljson.Marshal(r)
}

// ManifestEntry represents a single entry in the manifest.json of a Docker image tar archive
type ManifestEntry struct {
	Config string   `json:"Config"`
	Layers []string `json:"Layers"`
}

// ImageConfig represents the subset of config fields relevant for hashing present in a Docker
// image tar archive
type ImageConfig struct {
	Config ConfigOptions `json:"config"`
	RootFs RootFs        `json:"rootfs"`
}

func UnmarshalImageConfig(reader io.Reader) (ImageConfig, error) {
	var r ImageConfig
	decoder := json.NewDecoder(reader)
	err := decoder.Decode(&r)
	return r, err
}

func (r *ImageConfig) Marshal() ([]byte, error) {
	return canonicaljson.Marshal(r)
}

type ConfigOptions struct {
	User       string            `json:"User"`
	Env        []string          `json:"Env"`
	Cmd        []string          `json:"Cmd"`
	Entrypoint []string          `json:"Entrypoint"`
	WorkingDir string            `json:"WorkingDir"`
	Labels     map[string]string `json:"Labels,omitempty"`
}

type RootFs struct {
	Type    string   `json:"type"`
	DiffIds []string `json:"diff_ids"`
}

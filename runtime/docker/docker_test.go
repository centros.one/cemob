package docker

import (
	"archive/tar"
	"os"
	"testing"

	"gitlab.com/centros.one/cemob/hash"
)

func TestImageHash(t *testing.T) {
	hasher, err := hash.NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not get hasher: %v", err)
	}
	f, err := os.Open("_testdata/image.tar")
	if err != nil {
		t.Fatalf("Could not open image tar: %v", err)
	}
	defer f.Close()
	reader := tar.NewReader(f)
	hash, err := getImageHashFromReader(reader, "sha256:d1165f2212346b2bab48cb01c1e39ee8ad1be46b87873d9ca7a4e434980a7726", hasher)
	if err != nil {
		t.Fatalf("Could not get image hash: %v", err)
	}
	expected := "1ed56f409c18582118bfa8a2124ff5754c34558d6db9cf1d1766e02a7a60c232"
	if hash.Value() != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

package pkg

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-billy/v5/helper/mount"
	"github.com/go-git/go-billy/v5/helper/polyfill"
	"github.com/go-git/go-billy/v5/memfs"
	"github.com/go-git/go-billy/v5/osfs"
	"github.com/vbauerster/mpb/v6"
	"github.com/vbauerster/mpb/v6/decor"
	"gitlab.com/centros.one/cemob/config"
	"gitlab.com/centros.one/cemob/fs/statoverwrite"
	"gitlab.com/centros.one/cemob/hash"
	v1 "gitlab.com/centros.one/cemob/model/v1"
	"gitlab.com/centros.one/cemob/runtime/docker"
	"gitlab.com/centros.one/cemob/util"
)

// BuildFullOpts contain options for the build command
type BuildFullOpts struct {
	TrainOpts
	ValidateMRDOpts
	Tags     []string
	PushTags bool
}

// FillFromConfig sets option values from the global config
func (opts *BuildFullOpts) FillFromConfig(config config.Config) {
	opts.TrainOpts.FillFromConfig(config)
	// ValidateMTDOpts is part of TrainOpts and ValidateMRDOpts but the cmd only
	// initialized the one in the TrainOpts we so need to copy that into the
	// ValidateMRDOpts
	opts.ValidateMRDOpts.ValidateMTDOpts = opts.TrainOpts.ValidateMTDOpts
}

// Validate validates the options for the train command
func (opts *BuildFullOpts) Validate() error {
	if err := opts.TrainOpts.Validate(); err != nil {
		return err
	}
	if opts.PushTags && len(opts.Tags) == 0 {
		return errors.New("Push option provided without any tags")
	}
	return nil
}

type BuildResult struct {
	TrainResult
	MD      v1.MD
	ImageId string
}

// BuildFull performs model training according to the given training descriptor and build the image
func (cemob *CeMoB) BuildFull(opts *BuildFullOpts) (*BuildResult, error) {
	cemob.logger.Printf("Performing building of: %s", opts.ValidateMRDOpts.MRD)

	opts.ValidateMRDOpts.Silent = true
	mrd, err := cemob.ValidateMRD(&opts.ValidateMRDOpts)
	if err != nil {
		return nil, fmt.Errorf("Invalid mrd: %v", err)
	}
	trainResult, err := cemob.Train(&opts.TrainOpts)
	if err != nil {
		return nil, fmt.Errorf("Could not create model files: %v", err)
	}

	// update model file hashes
	for name, modelFile := range mrd.ModelFiles {
		resultFile, ok := trainResult.ModelFiles[name]
		if !ok {
			return nil, fmt.Errorf("Could not find model file \"%s\" in training result", name)
		}
		hash := v1.NewHash(resultFile.HashName, resultFile.HashValue)

		if err := validateModelHash(&modelFile, &resultFile); err != nil {
			return nil, fmt.Errorf("Could not validate model file \"%s\" hash: %v", name, err)
		}

		if !opts.keepExistingHashes {
			modelFile.Hash = &hash
		}
		// values are iterated by value so we need to update the map value explicitely
		mrd.ModelFiles[name] = modelFile
	}

	cemob.logger.Printf("Preparing build context")
	bcReader, bcWriter := io.Pipe()
	contextModelDir := "models"
	buildContextFs, err := getBuildContextFs(mrd, trainResult, opts.OutputDir, contextModelDir)
	if err != nil {
		return nil, fmt.Errorf("Could not create build context: %v", err)
	}

	excludes := getExcludes(contextModelDir, trainResult)
	filterFunc, err := util.WithExcludes(excludes)
	if err != nil {
		return nil, fmt.Errorf("Could not create build context excludes: %v", err)
	}

	go func() {
		defer bcWriter.Close()
		util.CreateTarballWithFilter(buildContextFs, bcWriter, filterFunc)
	}()

	cemob.logger.Printf("Starting service image build")
	cli, err := docker.NewDocker()
	if err != nil {
		return nil, err
	}
	defer cli.Close()
	for repository, auth := range opts.EnvAuth {
		cli.AddAuth(repository, auth.User, auth.Pass)
	}

	mtdMashal, err := trainResult.MTD.Marshal()
	if err != nil {
		return nil, fmt.Errorf("Could not marshal MTD: %v", err)
	}
	mrdMashal, err := mrd.Marshal()
	if err != nil {
		return nil, fmt.Errorf("Could not marshal MRD: %v", err)
	}
	labels := map[string]string{
		"one.centros.mtd": string(mtdMashal),
		"one.centros.mrd": string(mrdMashal),
	}

	var imageId string
	logCh, idCh, errCh := cli.ImageBuild(cemob.context, bcReader, types.ImageBuildOptions{
		Remove:      true,
		ForceRemove: true,
		NoCache:     true,
		Labels:      labels,
		Tags:        opts.Tags,
		Dockerfile:  "Dockerfile",
	})
	for out := range logCh {
		cemob.logger.Print(out)
	}
	if err = <-errCh; err != nil {
		return nil, err
	}
	select {
	case id := <-idCh:
		imageId = id
	default:
		return nil, errors.New("Docker daemon did not return image id")
	}

	cemob.logger.Printf("Calculating image hash for %s", imageId)
	imageHash, err := cli.GetImageHash(cemob.context, imageId, opts.Hash)
	if err != nil {
		return nil, fmt.Errorf("Could not calculate image hash: %v", err)
	}

	md := v1.MD{
		MTD:       trainResult.MTD,
		MRD:       mrd,
		ImageHash: imageHash,
	}
	mdMarshal, err := md.Marshal()
	if err != nil {
		return nil, fmt.Errorf("Could not marshal MD: %v", err)
	}
	hasher := cemob.baseHash.New()
	if _, err := hasher.Write(mdMarshal); err != nil {
		return nil, fmt.Errorf("Could not get MD hash: %v", err)
	}
	mdHash := cemob.baseHash.GetHash(hasher)
	mdName := fmt.Sprintf("%s.md", mdHash)
	mdFile, err := os.Create(path.Join(opts.OutputDir, mdName))
	if err != nil {
		return nil, fmt.Errorf("Could not open \"%s\" for writing: %v", mdName, err)
	}
	defer mdFile.Close()
	if _, err := mdFile.Write(mdMarshal); err != nil {
		return nil, fmt.Errorf("Could not write to \"%s\": %v", mdFile.Name(), err)
	}
	cemob.logger.Printf("MD written to %s", mdFile.Name())

	idName := fmt.Sprintf("%s.imageid", mdHash)
	idFile, err := os.Create(path.Join(opts.OutputDir, idName))
	if err != nil {
		return nil, fmt.Errorf("Could not open \"%s\" for writing: %v", idName, err)
	}
	defer idFile.Close()
	if _, err := io.WriteString(idFile, imageId); err != nil {
		return nil, fmt.Errorf("Could not write to \"%s\": %v", idFile.Name(), err)
	}

	if opts.PushTags {
		for _, tag := range opts.Tags {
			bars := make(map[string]*mpb.Bar)
			totals := make(map[string]int64)
			p := mpb.New(mpb.WithWidth(64), mpb.WithOutput(os.Stderr))
			cemob.logger.Printf("Pushing image tag: %s", tag)
			progressCh, errCh := cli.ImagePush(cemob.context, tag, types.ImagePushOptions{})
			for progress := range progressCh {
				id := progress.Id

				if id == "" || progress.ProgressDetail.Total == nil {
					continue
				}

				total := *progress.ProgressDetail.Total
				bar, ok := bars[id]
				if !ok {
					bar = p.AddBar(total,
						mpb.BarFillerClearOnComplete(),
						mpb.PrependDecorators(decor.Name(id)),
						mpb.AppendDecorators(decor.OnComplete(decor.Counters(decor.UnitKiB, "% .1f / % .1f"), "done!")))
					bars[id] = bar
					totals[id] = total
				}
				if progress.ProgressDetail.Current != nil {
					bar.SetCurrent(*progress.ProgressDetail.Current)
				}
			}
			if err = <-errCh; err != nil {
				for _, bar := range bars {
					bar.Abort(false)
				}
				// shutdown the bar container
				p.Wait()
				return nil, fmt.Errorf("Failed to push tag \"%s\": %v", tag, err)
			}
			// once the push is complete we make sure to finish the progress bars
			for id, bar := range bars {
				bar.SetTotal(totals[id], true)
			}
			p.Wait()
		}
	}

	// we want to print the id of the output image to stdout so that callers can easily read it
	cemob.logger.SetOutput(os.Stdout)
	cemob.logger.Println(imageId)
	cemob.logger.SetOutput(os.Stderr)

	result := BuildResult{
		TrainResult: *trainResult,
		MD:          md,
		ImageId:     imageId,
	}

	return &result, nil
}

func getBuildContextFs(mrd v1.MRD, trainResult *TrainResult, sourceModelDir, targetModelDir string) (billy.Filesystem, error) {
	bcFs := memfs.New()
	dockerFile, err := bcFs.Create("Dockerfile")
	if err != nil {
		return nil, fmt.Errorf("Could not create Dockerfile: %v", err)
	}
	dockerFileContent, err := getDockerfileContent(mrd, trainResult, targetModelDir)
	if err != nil {
		return nil, fmt.Errorf("Could not prepare Dockerfile content: %v", err)
	}
	if _, err := io.WriteString(dockerFile, dockerFileContent); err != nil {
		return nil, fmt.Errorf("Could not prepare Dockerfile: %v", err)
	}
	if err := bcFs.MkdirAll(targetModelDir, 777); err != nil {
		return nil, fmt.Errorf("Could not add models to build context: %v", err)
	}
	// model files should be readable by everyone
	var fileMode os.FileMode = 0444
	// so should any directories (even though we should not use them)
	var dirMode os.FileMode = 0555 | os.ModeDir
	modelFs := statoverwrite.New(osfs.New(sourceModelDir), &fileMode, &dirMode, nil, nil)
	bcFs = polyfill.New(mount.New(bcFs, targetModelDir, modelFs))
	return bcFs, nil
}

func getDockerfileContent(mrd v1.MRD, trainResult *TrainResult, modelDir string) (string, error) {
	var b strings.Builder
	fmt.Fprintf(&b, "FROM %s\n", mrd.SoftwareEnvironment.ImageReference())
	modelFiles := make([]string, 0, len(mrd.ModelFiles))
	for f := range mrd.ModelFiles {
		modelFiles = append(modelFiles, f)
	}
	// we need to make sure we copy the files in a defined order
	sort.Strings(modelFiles)
	for _, f := range modelFiles {
		fileTarget := mrd.ModelFiles[f]
		fileResult := trainResult.ModelFiles[f]
		fileTargetBasePath := filepath.Dir(fileTarget.Path)
		// there seem to be differences between different Docker environments when creating a new folder as
		// part of a COPY statement. In some environments the new folder in the created filesystem layer will
		// contain a whiteout file (".wh..wh..opq") in others it will not. This will result in a different
		// layer file system and therefore a different image hash. To avoid these problems we ensure  that the
		// target folder already exists in the base image and cancel the build if it doesn't
		fmt.Fprintf(&b, "RUN ls \"%s\" > /dev/null 2>&1 || { echo \"Target folder '%s' for model file '%s' does not exist in base image.\"; exit 1; }\n", fileTargetBasePath, fileTargetBasePath, f)
		fmt.Fprintf(&b, "COPY %s/%s %s\n", modelDir, filepath.Base(fileResult.Path), fileTarget.Path)
	}
	return b.String(), nil
}

func getExcludes(modelDir string, trainResult *TrainResult) []string {
	excludes := make([]string, 0, len(trainResult.ModelFiles)+1)
	excludes = append(excludes, fmt.Sprintf("%s/**", modelDir))
	for _, f := range trainResult.ModelFiles {
		filename := filepath.Base(f.Path)
		excludes = append(excludes, fmt.Sprintf("!%s/%s", modelDir, filename))
	}
	return excludes
}

func validateModelHash(modelFile *v1.FileTarget, resultFile *FileResult) error {
	if modelFile.Hash != nil {
		actualValue := resultFile.HashValue
		if modelFile.Hash.Name() != resultFile.HashName {
			hasher, err := hash.NewHash(modelFile.Hash.Name())
			if err != nil {
				return err
			}
			actualValue, err = getFileHash(resultFile.Path, hasher)
			if err != nil {
				return err
			}
		}
		if modelFile.Hash.Value() != actualValue {
			return fmt.Errorf("Invalid hash value, got: %s, want: %s", actualValue, modelFile.Hash.Value())
		}
	}
	return nil
}

func getFileHash(filePath string, hasher hash.Hash) (string, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer f.Close()
	h := hasher.New()
	if _, err := io.Copy(h, f); err != nil {
		return "", err
	}
	return hasher.GetHash(h), nil
}

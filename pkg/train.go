package pkg

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"sort"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-billy/v5/memfs"
	"github.com/go-git/go-billy/v5/osfs"
	"gitlab.com/centros.one/cemob/config"
	"gitlab.com/centros.one/cemob/hash"
	v1 "gitlab.com/centros.one/cemob/model/v1"
	"gitlab.com/centros.one/cemob/runtime/docker"
	"gitlab.com/centros.one/cemob/util"
	"gitlab.com/centros.one/cemob/vcs"
)

// TrainOpts contain options for the train command
type TrainOpts struct {
	ValidateMTDOpts
	CodeAuth           config.RepoAuth
	DataAuth           config.RepoAuth
	EnvAuth            map[string]config.EnvAuth
	Copy               bool
	WorkingDir         string
	OutputDir          string
	Hash               hash.Hash
	keepExistingHashes bool
}

// FillFromConfig sets option values from the global config
func (opts *TrainOpts) FillFromConfig(config config.Config) {
	opts.EnvAuth = config.EnvAuth
	opts.CodeAuth = getEffectiveAuth(opts.CodeAuth, config.RepoAuth, "code")
	opts.DataAuth = getEffectiveAuth(opts.DataAuth, config.RepoAuth, "data")
	opts.Copy = opts.Copy || config.TrainConfig.CopyRepos
	if opts.WorkingDir == "" {
		opts.WorkingDir = config.WorkingDir
	}
	if opts.OutputDir == "" {
		opts.OutputDir = config.OutputDir
	}
	opts.Hash = config.Hash
}

func getEffectiveAuth(existing config.RepoAuth, authMap map[string]config.RepoAuth, name string) config.RepoAuth {
	if existing.User == "" && existing.Pass == "" && existing.KeyPath == "" && existing.KeyPass == "" {
		for _, candidate := range []string{name, "default"} {
			auth, ok := authMap[candidate]
			if ok {
				return auth
			}
		}
	}
	return existing
}

// Validate validates the options for the train command
func (opts *TrainOpts) Validate() error {
	if err := opts.CodeAuth.Validate(); err != nil {
		return fmt.Errorf("Invalid code repo auth options: %v", err)
	}
	if err := opts.DataAuth.Validate(); err != nil {
		return fmt.Errorf("Invalid data repo auth options: %v", err)
	}
	if err := cleanFolder(&opts.WorkingDir); err != nil {
		return fmt.Errorf("Invalid working directory \"%s\": %v", opts.WorkingDir, err)
	}
	if err := cleanFolder(&opts.OutputDir); err != nil {
		return fmt.Errorf("Invalid output directory \"%s\": %v", opts.OutputDir, err)
	}
	return nil
}

func cleanFolder(path *string) error {
	cleaned, err := filepath.Abs(*path)
	if err != nil {
		return err
	}
	info, err := os.Stat(cleaned)
	if err != nil {
		return err
	}
	if !info.IsDir() {
		return errors.New("Not a folder")
	}
	*path = cleaned
	return nil
}

type TrainResult struct {
	ModelFiles map[string]FileResult
	MTD        v1.MTD
}

// Train performs model training according to the given training descriptor
func (cemob *CeMoB) Train(opts *TrainOpts) (*TrainResult, error) {
	mtd, err := cemob.ValidateMTD(&opts.ValidateMTDOpts)
	if err != nil {
		return nil, err
	}
	cemob.logger.Printf("Performing training of: %s", opts.MTD)

	cemob.logger.Printf("Cloning training code %s repository: %s", mtd.TrainingCode.Type, mtd.TrainingCode.Repo)
	codeDir, codeRepo, err := cemob.checkoutRepo(opts.WorkingDir, &mtd.TrainingCode, &opts.CodeAuth)
	if codeDir != "" {
		defer os.RemoveAll(codeDir)
	}
	if err != nil {
		return nil, err
	}

	cemob.logger.Printf("Calculating hash of code repository")
	codeRepoHash, err := validateRepoHash(&mtd.TrainingCode, codeDir, codeRepo, opts.Hash, opts.keepExistingHashes)
	if err != nil {
		return nil, fmt.Errorf("Could not calculate code repo hash: %v", err)
	}

	cemob.logger.Printf("Cloning training data %s repository: %s", mtd.TrainingData.Type, mtd.TrainingData.Repo)
	dataDir, dataRepo, err := cemob.checkoutRepo(opts.WorkingDir, &mtd.TrainingData, &opts.DataAuth)
	if dataDir != "" {
		defer os.RemoveAll(dataDir)
	}
	if err != nil {
		return nil, err
	}

	cemob.logger.Printf("Calculating hash of data repository")
	dataRepoHash, err := validateRepoHash(&mtd.TrainingData, dataDir, dataRepo, opts.Hash, opts.keepExistingHashes)
	if err != nil {
		return nil, fmt.Errorf("Could not calculate data repo hash: %v", err)
	}

	var codeArchive, dataArchive string
	if opts.Copy {
		cemob.logger.Print("Creating context for training code copy")
		codeArchive, err = createTarArchive(opts.WorkingDir, codeDir)
		if err != nil {
			return nil, err
		}
		defer os.Remove(codeArchive)

		cemob.logger.Print("Creating context for training data copy")
		dataArchive, err = createTarArchive(opts.WorkingDir, dataDir)
		if err != nil {
			return nil, err
		}
		defer os.Remove(dataArchive)
	}

	containerCodeDir := "/cemob/code"
	containerDataDir := path.Join(containerCodeDir, "data")

	cemob.logger.Printf("Peparing %d training parameter(s)", len(mtd.TrainingParameters))
	parameterEnv := make([]string, 0, len(mtd.TrainingParameters))
	parameterMounts := make([]mount.Mount, 0, len(mtd.TrainingParameters))
	parameterFs := memfs.New()
	for _, param := range mtd.TrainingParameters {
		if param.Env != "" {
			parameterEnv = append(parameterEnv, fmt.Sprintf("%s=%s", param.Env, param.Value))
		}
		if param.Path != "" {
			targetPath := param.Path
			if !path.IsAbs(targetPath) {
				targetPath = path.Join(containerCodeDir, targetPath)
			}
			if opts.Copy {
				if err := addParameterFile(targetPath, param.Value, parameterFs); err != nil {
					return nil, fmt.Errorf("Could not prepare parameter with path \"%s\" for copying: %v", param.Path, err)
				}
			} else {
				mount, err := createParameterMount(opts.WorkingDir, targetPath, param.Value)
				if err != nil {
					return nil, fmt.Errorf("Could not create parameter for path \"%s\": %v", param.Path, err)
				}
				// clean up the temporary file once we are done
				defer os.Remove(mount.Source)
				parameterMounts = append(parameterMounts, *mount)
			}

		}
	}
	// in case we need to copy the parameters we need to create the tar archive that will be copied over
	var parameterTarBuffer bytes.Buffer
	if opts.Copy {
		if err := util.CreateTarball(parameterFs, &parameterTarBuffer); err != nil {
			return nil, err
		}
	}

	cli, err := docker.NewDocker()
	if err != nil {
		return nil, err
	}
	defer cli.Close()
	for repository, auth := range opts.EnvAuth {
		cli.AddAuth(repository, auth.User, auth.Pass)
	}

	var cmd []string
	switch mtd.SoftwareEnvironment.ImageType {
	case v1.DvcPipeline:
		cmd = []string{"dvc", "repro"}
	default:
		return nil, fmt.Errorf("Unknown software environment image type: %s", mtd.SoftwareEnvironment.ImageType)
	}

	var mounts []mount.Mount
	if !opts.Copy {
		mounts = append(mounts, []mount.Mount{
			{
				Type:   mount.TypeBind,
				Source: codeDir,
				Target: containerCodeDir,
			},
			{
				Type:   mount.TypeBind,
				Source: dataDir,
				Target: containerDataDir,
			},
		}...)
		mounts = append(mounts, parameterMounts...)
	}

	cemob.logger.Printf("Creating training container with image: %s", mtd.SoftwareEnvironment)
	containerId, err := cli.CreateContainer(cemob.context, container.Config{
		AttachStdout: true,
		AttachStderr: true,
		Env:          parameterEnv,
		Tty:          false,
		Image:        mtd.SoftwareEnvironment.ImageReference(),
		Cmd:          cmd,
		WorkingDir:   containerCodeDir,
	}, container.HostConfig{
		AutoRemove: false,
		Mounts:     mounts,
	})
	if err != nil {
		return nil, err
	}

	defer cli.ContainerRemove(cemob.context, containerId, types.ContainerRemoveOptions{
		RemoveVolumes: true,
		Force:         true,
	})

	cemob.logger.Printf("Container created with id: %s", containerId)

	if opts.Copy {
		cemob.logger.Printf("Copying code repo to: %s", containerCodeDir)
		if err := cemob.copyTarToContainer(cli, containerId, codeArchive, containerCodeDir); err != nil {
			return nil, err
		}

		cemob.logger.Printf("Copying data repo to: %s", containerDataDir)
		if err := cemob.copyTarToContainer(cli, containerId, dataArchive, containerDataDir); err != nil {
			return nil, err
		}

		cemob.logger.Print("Copying parameters")
		if err := cli.CopyToContainer(cemob.context, containerId, "/", &parameterTarBuffer, types.CopyToContainerOptions{}); err != nil {
			return nil, err
		}
	}

	cemob.logger.Printf("Running training command: %v", cmd)
	outCh, errCh := cli.RunContainer(cemob.context, containerId)
	for out := range outCh {
		cemob.logger.Print(out)
	}
	if err = <-errCh; err != nil {
		return nil, err
	}

	result := &TrainResult{
		ModelFiles: make(map[string]FileResult),
	}

	for name, modelFile := range mtd.ModelFiles {
		sourcePath := modelFile.FromFile
		cemob.logger.Printf("Copying model file \"%s\" from container", sourcePath)
		if !path.IsAbs(sourcePath) {
			sourcePath = path.Join(containerCodeDir, sourcePath)
		}
		sourcePath = path.Clean(sourcePath)
		tarReader, err := cli.CopyFromContainer(cemob.context, containerId, sourcePath)
		if err != nil {
			return nil, err
		}
		defer tarReader.Close()

		header, err := tarReader.Next()
		switch {
		// if no more files are found return
		case err == io.EOF || header == nil:
			return nil, fmt.Errorf("No file found")
		// return any other error
		case err != nil:
			return nil, err
		}

		targetPath := path.Clean(opts.OutputDir)
		modelFile, err := ioutil.TempFile(targetPath, "cemob-model-*")
		if err != nil {
			return nil, err
		}
		defer modelFile.Close()

		hw, err := hash.NewHashingWriter(modelFile, opts.Hash)
		if err != nil {
			return nil, err
		}

		io.Copy(hw, &tarReader.Reader)
		h := hw.GetHash()
		cemob.logger.Printf("Model file written to %s (hash: %s)", modelFile.Name(), h)
		result.ModelFiles[name] = FileResult{
			Path:      modelFile.Name(),
			HashName:  opts.Hash.Name(),
			HashValue: h,
		}
	}

	mtd.TrainingCode.Hash = codeRepoHash
	mtd.TrainingData.Hash = dataRepoHash

	mtdJson, err := mtd.Marshal()
	if err != nil {
		return nil, fmt.Errorf("Could not serialize MTD json: %v", err)
	}
	hasher := cemob.baseHash.New()
	if _, err := hasher.Write(mtdJson); err != nil {
		return nil, fmt.Errorf("Could not get MTD hash: %v", err)
	}
	mtdName := fmt.Sprintf("%s.mtd", cemob.baseHash.GetHash(hasher))
	mtdFile, err := os.Create(path.Join(opts.OutputDir, mtdName))
	if err != nil {
		return nil, fmt.Errorf("Could not open \"%s\" for writing: %v", mtdName, err)
	}
	defer mtdFile.Close()
	if _, err := mtdFile.Write(mtdJson); err != nil {
		return nil, fmt.Errorf("Could not write to \"%s\": %v", mtdFile.Name(), err)
	}
	cemob.logger.Printf("MTD written to %s", mtdFile.Name())

	result.MTD = mtd

	return result, nil
}

func (cemob *CeMoB) checkoutRepo(workingDir string, source *v1.CodeDataSource, auth *config.RepoAuth) (string, vcs.Repo, error) {
	tempDir, err := ioutil.TempDir(workingDir, "cemob-co-*")
	if err != nil {
		return "", nil, err
	}

	var repo vcs.Repo

	switch source.Type {
	case v1.Git:
		{
			switch {
			case auth.User != "":
				{
					repo = vcs.NewGitRepoWithUserPass(tempDir, auth.User, auth.Pass)
				}
			case auth.KeyPath != "":
				{
					repo = vcs.NewGitRepoWithPublicKeyAuth(tempDir, auth.KeyPath, auth.KeyPass)
				}
			default:
				{
					repo = vcs.NewGitRepo(tempDir)
				}
			}
		}
	case v1.DvcLocal:
		{
			switch {
			case auth.User != "":
				{
					repo = vcs.NewDvcRepoWithUserPass(tempDir, auth.User, auth.Pass)
				}
			case auth.KeyPath != "":
				{
					repo = vcs.NewDvcRepoWithPublicKeyAuth(tempDir, auth.KeyPath, auth.KeyPass)
				}
			default:
				{
					repo = vcs.NewDvcRepo(tempDir)
				}
			}
		}
	}
	err = repo.Initialize(source.Repo)
	if err != nil {
		return tempDir, nil, err
	}
	cemob.logger.Printf("Checking out revision: %s", source.Revision)
	err = repo.Checkout(source.Revision)
	if err != nil {
		return tempDir, nil, err
	}
	return tempDir, repo, nil
}

func (cemob *CeMoB) copyTarToContainer(cli *docker.Docker, containerId, tarArchivePath, targetDir string) error {
	tarArchive, err := os.Open(tarArchivePath)
	if err != nil {
		return fmt.Errorf("Cannot open copy context \"%s\": %v", tarArchivePath, err)
	}
	defer tarArchive.Close()
	err = cli.CopyToContainer(cemob.context, containerId, targetDir, tarArchive, types.CopyToContainerOptions{
		AllowOverwriteDirWithFile: true,
	})
	if err != nil {
		return fmt.Errorf("Could not copy files: %v", err)
	}
	return nil
}

func validateRepoHash(source *v1.CodeDataSource, repoPath string, repo vcs.Repo, baseHash hash.Hash, keepExistingHash bool) (*v1.Hash, error) {
	var excludes []string
	var h hash.Hash
	var existingHashName string
	var existingHashValue string
	var err error
	if source.Hash != nil && source.Hash.CombinedValue != "" {
		excludes = source.Hash.Excludes
		existingHashName = source.Hash.Name()
		existingHashValue = source.Hash.Value()
		h, err = hash.NewHash(existingHashName)
		if err != nil {
			return nil, err
		}
	} else {
		excludes = repo.GetExcludeList()
		h = baseHash
	}
	compareHash, err := getRepoHash(repoPath, excludes, h)
	if err != nil {
		return nil, err
	}
	if existingHashValue != "" && compareHash.Value() != existingHashValue {
		return nil, fmt.Errorf("Invalid hash value, got: %s, want: %s", compareHash.Value(), existingHashValue)
	}
	if !keepExistingHash && existingHashName != "" && baseHash.Name() != existingHashName {
		compareHash, err = getRepoHash(repoPath, excludes, baseHash)
		if err != nil {
			return nil, err
		}
	}
	return compareHash, nil
}

func createParameterMount(workingDir, targetPath, value string) (*mount.Mount, error) {
	paramFile, err := ioutil.TempFile(workingDir, "cemob-param-")
	if err != nil {
		return nil, err
	}
	defer paramFile.Close()
	_, err = paramFile.WriteString(value)
	if err != nil {
		return nil, err
	}
	return &mount.Mount{
		Type:   mount.TypeBind,
		Source: paramFile.Name(),
		Target: targetPath,
	}, nil
}

func addParameterFile(targetPath, value string, parameterFs billy.Filesystem) error {
	baseDir, _ := path.Split(targetPath)
	if err := parameterFs.MkdirAll(baseDir, 0777); err != nil {
		return err
	}
	file, err := parameterFs.Create(targetPath)
	if err != nil {
		return err
	}
	_, err = io.WriteString(file, value)
	return err
}

func createTarArchive(workingDir, sourcePath string) (string, error) {
	archiveFile, err := ioutil.TempFile(workingDir, "cemob-copy-context-")
	if err != nil {
		return "", err
	}
	defer archiveFile.Close()
	fs := osfs.New(sourcePath)
	err = util.CreateTarball(fs, archiveFile)
	if err != nil {
		return "", err
	}
	return archiveFile.Name(), nil
}

func getRepoHash(path string, excludeList []string, hasher hash.Hash) (*v1.Hash, error) {
	fs := osfs.New(path)
	// we make sure the exclude list is sorted to make the output of the list predictable
	// when serializing it to json later
	sort.Strings(excludeList)
	filter, err := util.WithExcludes(excludeList)
	if err != nil {
		return nil, err
	}
	hash, err := hash.DirhashWithFilter(fs, "/", hasher, hasher, hasher, filter)
	if err != nil {
		return nil, err
	}
	h := v1.NewHashWithExcludes(hasher.Name(), hash, excludeList)
	return &h, nil
}

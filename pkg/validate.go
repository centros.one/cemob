package pkg

import (
	"fmt"

	"github.com/hashicorp/go-multierror"
	"github.com/xeipuuv/gojsonschema"
	v1 "gitlab.com/centros.one/cemob/model/v1"
)

// ValidateMTDOpts contain options for the validate mtd command
type ValidateMTDOpts struct {
	MTD    FileSource
	Silent bool
}

// ValidateMTD performs validation of Model Training Descriptor
func (cemob *CeMoB) ValidateMTD(opts *ValidateMTDOpts) (v1.MTD, error) {
	var mtd v1.MTD
	if !opts.Silent {
		cemob.logger.Printf("Performing validation of: %s", opts.MTD)
	}
	if err := opts.MTD.ReadSource(); err != nil {
		return mtd, err
	}
	additionalSchemas := cemob.getCommonSchemas()
	if err := validateForSchema(cemob.trainingDescriptorSchema, opts.MTD.sourceContents, additionalSchemas); err != nil {
		return mtd, err
	}
	mtd, err := v1.UnmarshalMtd(opts.MTD.sourceContents)
	if err != nil {
		return mtd, err
	}
	if err := mtd.Validate(); err != nil {
		return mtd, err
	}
	if !opts.Silent {
		cemob.logger.Println("The document is valid")
	}
	return mtd, nil
}

// ValidateMRDOpts contain options for the validate mrd command
type ValidateMRDOpts struct {
	ValidateMTDOpts
	MRD FileSource
}

// ValidateMRD performs validation of Model Runtime Descriptor
func (cemob *CeMoB) ValidateMRD(opts *ValidateMRDOpts) (v1.MRD, error) {
	var mrd v1.MRD
	if !opts.Silent {
		cemob.logger.Printf("Performing validation of: %s", opts.MRD)
	}
	if err := opts.MRD.ReadSource(); err != nil {
		return mrd, err
	}
	additionalSchemas := cemob.getCommonSchemas()
	if err := validateForSchema(cemob.runtimeDescriptorSchema, opts.MRD.sourceContents, additionalSchemas); err != nil {
		return mrd, err
	}
	mrd, err := v1.UnmarshalMrd(opts.MRD.sourceContents)
	if err != nil {
		return mrd, err
	}
	if err := mrd.Validate(); err != nil {
		return mrd, err
	}
	if opts.MTD.SourcePath != "" {
		silent := opts.Silent
		opts.Silent = true
		mtd, err := cemob.ValidateMTD(&opts.ValidateMTDOpts)
		if err != nil {
			return mrd, fmt.Errorf("Invalid mtd: %v", err)
		}
		opts.Silent = silent
		missing := make([]string, 0, len(mrd.ModelFiles))
		for f := range mrd.ModelFiles {
			if _, ok := mtd.ModelFiles[f]; !ok {
				missing = append(missing, f)
			}
		}
		if len(missing) > 0 {
			return mrd, fmt.Errorf("The following model files referenced in mrd are missing from mtd: %v", missing)
		}
	}
	if !opts.Silent {
		cemob.logger.Println("The document is valid")
	}
	return mrd, nil
}

// ValidateMDOpts contain options for the validate md command
type ValidateMDOpts struct {
	MD FileSource
}

// ValidateMRD performs validation of Model Descriptor
func (cemob *CeMoB) ValidateMD(opts *ValidateMDOpts) (v1.MD, error) {
	var md v1.MD
	cemob.logger.Printf("Performing validation of: %s", opts.MD)
	if err := opts.MD.ReadSource(); err != nil {
		return md, err
	}
	additionalSchemas := cemob.getCommonSchemas()
	additionalSchemas["/mtd.json"] = cemob.trainingDescriptorSchema
	additionalSchemas["/mrd.json"] = cemob.runtimeDescriptorSchema
	if err := validateForSchema(cemob.modelDescriptorSchema, opts.MD.sourceContents, additionalSchemas); err != nil {
		return md, err
	}
	md, err := v1.UnmarshalMd(opts.MD.sourceContents)
	if err != nil {
		return md, err
	}
	if err := md.Validate(); err != nil {
		return md, err
	}
	cemob.logger.Println("The document is valid")
	return md, nil
}

func (cemob *CeMoB) getCommonSchemas() map[string]string {
	return map[string]string{
		"/common.json": cemob.commonSchema,
	}
}

func validateForSchema(schema string, contents []byte, additionalSchemas map[string]string) error {
	sl := gojsonschema.NewSchemaLoader()
	for name, content := range additionalSchemas {
		loader := gojsonschema.NewStringLoader(content)
		sl.AddSchema(name, loader)
	}
	loader := gojsonschema.NewStringLoader(schema)
	compiled, err := sl.Compile(loader)
	if err != nil {
		return err
	}
	result, err := compiled.Validate(gojsonschema.NewBytesLoader(contents))
	if err != nil {
		return err
	}
	if !result.Valid() {
		for _, e := range result.Errors() {
			err = multierror.Append(err, fmt.Errorf("%s", e))
		}
		return err
	}
	return nil
}

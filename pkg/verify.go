package pkg

import (
	"fmt"

	"gitlab.com/centros.one/cemob/config"
	"gitlab.com/centros.one/cemob/hash"
	v1 "gitlab.com/centros.one/cemob/model/v1"
)

// VerifyOpts contain options for the verify command
type VerifyOpts struct {
	MD            FileSource
	BuildFullOpts BuildFullOpts
}

// FillFromConfig sets option values from the global config
func (opts *VerifyOpts) FillFromConfig(config config.Config) {
	opts.BuildFullOpts.FillFromConfig(config)
}

// Validate validates the options for the verify command
func (opts *VerifyOpts) Validate() error {
	return opts.BuildFullOpts.Validate()
}

type VerifyResult struct {
	BuildResult
	OriginalMD v1.MD
}

// Verify performs verification of a model descriptor
func (cemob *CeMoB) Verify(opts *VerifyOpts) (*VerifyResult, error) {
	cemob.logger.Printf("Performing verification of: %s", opts.MD)
	md, err := cemob.ValidateMD(&ValidateMDOpts{
		MD: opts.MD,
	})
	if err != nil {
		return nil, err
	}
	mtdBytes, err := md.MTD.Marshal()
	if err != nil {
		return nil, fmt.Errorf("Could not marshal MTD: %v", err)
	}
	// set MTD from MD
	mtdName := fmt.Sprintf("%s#/mtd", opts.MD.SourcePath)
	opts.BuildFullOpts.TrainOpts.ValidateMTDOpts.MTD.SourcePath = mtdName
	opts.BuildFullOpts.TrainOpts.ValidateMTDOpts.MTD.sourceContents = mtdBytes
	opts.BuildFullOpts.ValidateMRDOpts.ValidateMTDOpts.MTD.SourcePath = mtdName
	opts.BuildFullOpts.ValidateMRDOpts.ValidateMTDOpts.MTD.sourceContents = mtdBytes
	mrdBytes, err := md.MRD.Marshal()
	if err != nil {
		return nil, fmt.Errorf("Could marshal MRD: %v", err)
	}
	// set MRD from MD
	mrdName := fmt.Sprintf("%s#/mrd", opts.MD.SourcePath)
	opts.BuildFullOpts.ValidateMRDOpts.MRD.SourcePath = mrdName
	opts.BuildFullOpts.ValidateMRDOpts.MRD.sourceContents = mrdBytes
	// set the hasher to the one from MD
	hasher, err := hash.NewHash(md.ImageHash.Name())
	if err != nil {
		return nil, err
	}
	opts.BuildFullOpts.Hash = hasher
	// make sure we keep existing hashes to get the same MRD and MTD and therefore
	// the same image id
	opts.BuildFullOpts.keepExistingHashes = true
	// perform image build
	buildResult, err := cemob.BuildFull(&opts.BuildFullOpts)
	if err != nil {
		return nil, err
	}
	cemob.logger.Printf("Comparing image hash values")
	imageHashValue := buildResult.MD.ImageHash.Value()
	if imageHashValue != md.ImageHash.Value() {
		return nil, fmt.Errorf("Invalid hash value, got: %s, want: %s", imageHashValue, md.ImageHash.Value())
	}
	result := VerifyResult{
		BuildResult: *buildResult,
		OriginalMD:  md,
	}
	cemob.logger.Printf("Image hash \"%s\" matches", imageHashValue)
	return &result, nil
}

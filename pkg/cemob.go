package pkg

import (
	"context"

	// we need to embed schema files here
	_ "embed"
	"log"
	"os"

	"gitlab.com/centros.one/cemob/hash"
)

var version = "?"
var versionHash = "?"

// CeMoB is the logic behind the CeMoB client
type CeMoB struct {
	version                  string
	versionHash              string
	commonSchema             string
	trainingDescriptorSchema string
	runtimeDescriptorSchema  string
	modelDescriptorSchema    string
	logger                   *log.Logger
	context                  context.Context
	baseHash                 hash.Hash
}

//go:embed schemas/mtd.json
var mtd string

//go:embed schemas/mrd.json
var mrd string

//go:embed schemas/md.json
var md string

//go:embed schemas/common.json
var common string

// New creates a new instance of the CeMoB client
func New() CeMoB {
	logger := log.New(os.Stderr, "", 0)
	baseHash, err := hash.NewHash("sha256")
	if err != nil {
		panic("Could not initialize base hash")
	}
	return CeMoB{
		version:                  version,
		versionHash:              versionHash,
		commonSchema:             common,
		trainingDescriptorSchema: mtd,
		runtimeDescriptorSchema:  mrd,
		modelDescriptorSchema:    md,
		logger:                   logger,
		context:                  context.Background(),
		baseHash:                 baseHash,
	}
}

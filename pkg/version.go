package pkg

import (
	"fmt"
)

// Version returns the version string
func (cemob *CeMoB) Version() string {
	return fmt.Sprintf("%s (%s)", cemob.version, cemob.versionHash)
}

package pkg

import (
	"fmt"
	"os"
	"path/filepath"
)

// FileResult represents a single file that is the result of an operation
type FileResult struct {
	Path      string
	HashName  string
	HashValue string
}

// FileSource represents a single file that is the input of an operation
type FileSource struct {
	SourcePath     string
	sourceContents []byte
}

func (fs FileSource) String() string {
	fullPath, err := filepath.Abs(fs.SourcePath)
	if err != nil {
		return fmt.Sprintf("<Err: %s>", err)
	}
	return fullPath
}

// ReadSource sets the sourceContents from the file pointed at by sourcePath
func (fs *FileSource) ReadSource() error {
	if fs.sourceContents != nil {
		return nil
	}
	contents, err := os.ReadFile(fs.SourcePath)
	if err != nil {
		return err
	}
	fs.sourceContents = contents
	return nil
}

package main

import (
	"os"

	"gitlab.com/centros.one/cemob/cmd"
)

func main() {
	cmd := cmd.BuildRootCommand()
	if err := cmd.Execute(); err != nil {
		os.Exit(1)
	}
}

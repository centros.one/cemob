package hash

import (
	"fmt"
	"io"
	"testing"
)

func TestPassthroughHashAvailable(t *testing.T) {
	hash := NewPassthroughHash(64)
	if !hash.Available() {
		t.Errorf("Passthrough hash not available")
	}
}

func TestPassthroughHashName(t *testing.T) {
	hash := NewPassthroughHash(64)
	expected := "passthrough"
	if expected != hash.Name() {
		t.Errorf("Name was incorrect, got: %s, want: %s.", hash.Name(), expected)
	}
}

func TestPassthroughHash(t *testing.T) {
	hash := NewPassthroughHash(64)
	value := "testvalue"
	if !hash.Available() {
		t.Fatalf("Passthrough hash not available")
	}
	h := hash.New()
	if _, err := io.WriteString(h, value); err != nil {
		t.Fatalf("Could not write value to hash")
	}
	slice := make([]byte, 0, 64)
	hashValue := string(h.Sum(slice))
	if hashValue != value {
		t.Errorf("Value was incorrect, got: %s, want: %s.", hashValue, value)
	}
}

func TestPassthroughHashExistingSlice(t *testing.T) {
	hash := NewPassthroughHash(64)
	value := "testvalue"
	if !hash.Available() {
		t.Fatalf("Passthrough hash not available")
	}
	h := hash.New()
	if _, err := io.WriteString(h, value); err != nil {
		t.Fatalf("Could not write value to hash")
	}
	existingValue := "existing value, hash: "
	slice := []byte(existingValue)
	expected := fmt.Sprintf("%s%s", existingValue, value)
	hashValue := string(h.Sum(slice))
	if hashValue != expected {
		t.Errorf("Value was incorrect, got: %s, want: %s.", hashValue, expected)
	}
}

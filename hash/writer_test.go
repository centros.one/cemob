package hash

import (
	_ "crypto/sha256"
	"io"
	"testing"
)

func TestHashingWriter(t *testing.T) {
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	writer, err := NewHashingWriter(io.Discard, hasher)
	if err != nil {
		t.Fatalf("Could not create writer: %v", err)
	}
	io.WriteString(writer, "a")
	hash := writer.GetHash()
	expected := "ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

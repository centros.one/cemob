package hash

import (
	"crypto"
	"encoding/hex"
	"fmt"
	"hash"
)

// Hash is a wrapper for crypto.Hash that includes the hash name
type Hash interface {
	Available() bool
	New() hash.Hash
	Name() string
	GetHash(hasher hash.Hash) string
}

type hashWrapper struct {
	hash crypto.Hash
	name string
}

func (h *hashWrapper) Available() bool {
	return h.hash.Available()
}

func (h *hashWrapper) New() hash.Hash {
	return h.hash.New()
}

func (h *hashWrapper) Name() string {
	return h.name
}

func (h *hashWrapper) GetHash(hasher hash.Hash) string {
	hash := make([]byte, 0, hasher.Size())
	hash = hasher.Sum(hash)
	return hex.EncodeToString(hash)
}

type passthroughHash struct {
	buf  []byte
	size int
}

func NewPassthroughHash(size int) Hash {
	return &passthroughHash{
		size: size,
	}
}

func (h *passthroughHash) New() hash.Hash {
	return &passthroughHash{
		buf:  make([]byte, 0, h.size),
		size: h.size,
	}
}

func (h *passthroughHash) Available() bool {
	return true
}

func (h *passthroughHash) Name() string {
	return "passthrough"
}

func (d *passthroughHash) Reset() {
	d.buf = make([]byte, 0, d.size)
}

func (d *passthroughHash) Size() int {
	return d.size
}

func (d *passthroughHash) BlockSize() int {
	return d.size
}

func (d *passthroughHash) Write(p []byte) (nn int, err error) {
	d.buf = append(d.buf, p...)
	return len(p), nil
}

func (d *passthroughHash) Sum(in []byte) []byte {
	return append(in, d.buf...)
}

func (h *passthroughHash) GetHash(hasher hash.Hash) string {
	hash := make([]byte, 0, hasher.Size())
	hash = hasher.Sum(hash)
	return string(hash)
}

var hashes = map[string]crypto.Hash{
	"sha256": crypto.SHA256,
	"sha512": crypto.SHA512,
}

func NewHash(name string) (Hash, error) {
	cryptoHash, ok := hashes[name]
	if !ok {
		return nil, fmt.Errorf("Unkown hash name: %s", name)
	}
	return &hashWrapper{
		name: name,
		hash: cryptoHash,
	}, nil
}

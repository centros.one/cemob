package hash

import (
	"fmt"
	"hash"
	"io"
	"io/fs"
	"sort"

	"github.com/go-git/go-billy/v5"
	"gitlab.com/centros.one/cemob/util"
)

type byName []fs.FileInfo

func (a byName) Len() int           { return len(a) }
func (a byName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a byName) Less(i, j int) bool { return a[i].Name() < a[j].Name() }

// Dirhash calculates a recursive hash over the given filesystem
// root is the start path for the hash (if it points to a file the hash of
// this file will be returned). fileHasher is the hash used to create the
// hashes of encountered files, linkHasher will be used to create the hashes
// of encountered links while dirHasher will be used to calculate
// the hash for a directory from the hashes of its contents.
//
// The algorithm for calculating the directory hash is this:
// 1. Sort the list of entries in the directory by name
// 2. Calculate the hash for each entry
// 2a. If it is a file use the file hasher on the file content
// 2b. If it is a link use the link hasher on the link target and append ".link" suffix
//     to calculated hash
// 2c. If it is a directory calculate the hash recursively using this algorithm
// 3. Append the string "$HASH_SUM $ENTRY_NAME\n" to a buffer ($HASH_SUM is the hexstring of the
//    calculated hash, $ENTRY_NAME is the name of the entry within the directory)
// 4. Calculate the hash of the buffer using dir hasher
// 5. Return "$DIR_HASH.dir" where $DIR_HASH is the hexstring of the calculated dir hash
func Dirhash(fs billy.Filesystem, root string, fileHasher Hash, dirHasher Hash, linkHasher Hash) (string, error) {
	return DirhashWithFilter(fs, root, fileHasher, dirHasher, linkHasher, func(path string) bool { return true })
}

// DirhashWithFilter calculates a recursive hash over the given filesystem
// root is the start path for the hash (if it points to a file the hash of
// this file will be returned). fileHasher is the hash used to create the
// hashes of encountered files, linkHasher will be used to create the hashes
// of encountered links while dirHasher will be used to calculate the hash
// for a directory from the hashes of its contents. The given filter
// determines which files/folders to include in the hash
//
// The algorithm for calculating the directory hash is this:
// 1. Sort the list of entries in the directory by name
// 2. Calculate the hash for each entry for which the filter function returns true,
//    skip it otherwise
// 2a. If it is a file use the file hasher on the file content
// 2b. If it is a link use the link hasher on the link target and append ".link" suffix
//     to calculated hash
// 2c. If it is a directory calculate the hash recursively using this algorithm
// 3. Append the string "$HASH_SUM $ENTRY_NAME\n" to a buffer ($HASH_SUM is the hexstring of the
//    calculated hash, $ENTRY_NAME is the name of the entry within the directory)
// 4. Calculate the hash of the buffer using dir hasher
// 5. Return "$DIR_HASH.dir" where $DIR_HASH is the hexstring of the calculated dir hash
func DirhashWithFilter(fs billy.Filesystem, root string, fileHasher Hash, dirHasher Hash, linkHasher Hash, filter util.FilterFunc) (string, error) {
	if !fileHasher.Available() {
		return "", fmt.Errorf("fileHasher \"%s\" is not available", fileHasher.Name())
	}
	if !dirHasher.Available() {
		return "", fmt.Errorf("dirHasher \"%s\" is not available", dirHasher.Name())
	}
	if root != "" {
		// we don't want to follow links
		rootFile, err := fs.Lstat(root)
		if err != nil {
			return "", fmt.Errorf("Could not get info for \"%s\": %v", root, err)
		}
		if !rootFile.IsDir() {
			return filehash(fs, root, fileHasher, linkHasher)
		}
	}
	return dirhash(fs, root, fileHasher, dirHasher, linkHasher, filter)
}

func dirhash(fs billy.Filesystem, root string, fileHasher Hash, dirHasher Hash, linkHasher Hash, filter util.FilterFunc) (string, error) {
	entries, err := fs.ReadDir(root)
	if err != nil {
		return "", fmt.Errorf("Could not read dir \"%s\": %v", root, err)
	}
	sort.Sort(byName(entries))

	h := dirHasher.New()
	for _, entry := range entries {
		name := entry.Name()
		path := fs.Join(root, name)
		if !filter(path) {
			continue
		}
		var hash string
		if entry.IsDir() {
			hash, err = dirhash(fs, path, fileHasher, dirHasher, linkHasher, filter)
		} else {
			hash, err = filehash(fs, path, fileHasher, linkHasher)
		}
		if err != nil {
			return "", err
		}
		fmt.Fprintf(h, "%s %s\n", hash, name)
	}
	result := fmt.Sprintf("%s.dir", dirHasher.GetHash(h))
	return result, nil
}

func filehash(fs billy.Filesystem, path string, fileHasher Hash, linkHasher Hash) (string, error) {
	var hashSuffix string
	var fh hash.Hash
	var hasher Hash
	target, err := fs.Readlink(path)
	if err == nil {
		hasher = linkHasher
		fh = linkHasher.New()
		hashSuffix = ".link"
		// if the file is a link we calculate the hash value from its target
		if _, err := io.WriteString(fh, target); err != nil {
			return "", fmt.Errorf("Could not hash link \"%s\": %v", path, err)
		}
	} else {
		hasher = fileHasher
		fh = fileHasher.New()
		f, err := fs.Open(path)
		if err != nil {
			return "", fmt.Errorf("Could not open file \"%s\": %v", path, err)
		}
		defer f.Close()
		if _, err := io.Copy(fh, f); err != nil {
			return "", fmt.Errorf("Could not hash file \"%s\": %v", path, err)
		}
	}
	hash := hasher.GetHash(fh)
	return fmt.Sprintf("%s%s", hash, hashSuffix), nil
}

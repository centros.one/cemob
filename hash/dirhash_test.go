package hash

import (
	_ "crypto/sha256"
	_ "crypto/sha512"
	"io"
	"testing"

	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-billy/v5/memfs"
)

func TestFilehash(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := filehash(fs, "a", hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithFile(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "a", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithSingleFolder(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "b", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "2ed7acf5e8aff559997ece5dad07839e30f03e618f6ad7ca5a2ecf0f55f39ccf.dir"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithRecursiveFolder(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "e895ff18552bce08608dbd925037efc0548f583a44b76ce1a757dee478248b50.dir"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithEmptyFolder(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "d", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855.dir"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithSymlinkToFile(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	if err := fs.Symlink("a", "e"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "b86afaf6d040b9d33f8f2e3e00e6562c102dc32f3d2c8254c300b6be878f2d9f.dir"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithSymlinkToSymlinkToFile(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	if err := fs.Symlink("a", "e"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	if err := fs.Symlink("e", "f"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "e36737a05eaf89479e3c0aa57eaffaed5f59de6676ca85dabbeab8381d03a388.dir"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithSymlinkToFolder(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	if err := fs.Symlink("b", "e"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "03c2819dde7f66dc828f24acfb69b98aa671c9f712b48a3086669433a69f3a33.dir"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithSymlinkToSymlinkToFolder(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	if err := fs.Symlink("b", "e"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	if err := fs.Symlink("e", "f"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "180afeae8d4dc08137091e14706cf68f260ae9a41a9d6959738e453ddc96ffc1.dir"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithSymlinkAsRootToFolder(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	if err := fs.Symlink("b", "e"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "e", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "3e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d.link"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithSymlinkAsRootToSymlinkToFolder(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	if err := fs.Symlink("b", "e"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	if err := fs.Symlink("e", "f"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "f", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "3f79bb7b435b05321651daefd374cdc681dc06faa65e374e38337b88ca046dea.link"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithSymlinkAsRootToFile(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	if err := fs.Symlink("a", "e"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "e", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb.link"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithSymlinkAsRootToSymlinkToFile(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	if err := fs.Symlink("a", "e"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	if err := fs.Symlink("e", "f"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "f", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "3f79bb7b435b05321651daefd374cdc681dc06faa65e374e38337b88ca046dea.link"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithInvalidSymlink(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	if err := fs.Symlink("aa", "e"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "8af3cb990b70bb50ec00783c55501007da786b1ea2f336e9557a4edf9ad6d614.dir"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithInvalidRecursiveSymlink(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	if err := fs.Symlink("aa", "e"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	if err := fs.Symlink("e", "f"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "49784f50852af4d1f19f4b0c8ba3859c0416e7c7f17044c25b575d408e880e04.dir"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithInvalidSymlinkAsRoot(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	if err := fs.Symlink("aa", "e"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	hasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !hasher.Available() {
		t.Fatalf("Hasher %s not avilable", hasher.Name())
	}
	hash, err := Dirhash(fs, "e", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "961b6dd3ede3cb8ecbaacbd68de040cd78eb2ed5889130cceb4c49268ea4d506.link"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithPassthrough(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	hasher := NewPassthroughHash(10)
	hash, err := Dirhash(fs, "", hasher, hasher, hasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := `a a
b1 b1
b2 b2
b3 b3
.dir b
c c
.dir d
.dir`
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithPassthroughAndFolderHash(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	fileHasher := NewPassthroughHash(10)
	dirHasher, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher: %v", err)
	}
	if !dirHasher.Available() {
		t.Fatalf("Hasher %s not avilable", dirHasher.Name())
	}
	hash, err := Dirhash(fs, "", fileHasher, dirHasher, dirHasher)
	if err != nil {
		t.Fatalf("Could not calculate hash: %v", err)
	}
	expected := "73274a22b76bd08077cb7cf869dab222775aa4839154cd527558dad0052dd9ab.dir"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func TestDirhashWithDifferentHashers(t *testing.T) {
	fs, err := createTestFileSystem()
	if err != nil {
		t.Fatalf("Could not create test filesystem: %v", err)
	}
	if err := fs.Symlink("b1", "b/b4"); err != nil {
		t.Fatalf("Could not create symmlink: %v", err)
	}
	hasher1, err := NewHash("sha256")
	if err != nil {
		t.Fatalf("Could not create hasher1: %v", err)
	}
	if !hasher1.Available() {
		t.Fatalf("Hasher %s not avilable", hasher1.Name())
	}
	hasher2, err := NewHash("sha512")
	if err != nil {
		t.Fatalf("Could not create hasher2: %v", err)
	}
	if !hasher2.Available() {
		t.Fatalf("Hasher %s not avilable", hasher2.Name())
	}
	hash, err := Dirhash(fs, "b", hasher1, hasher2, hasher1)
	if err != nil {
		t.Errorf("Could not calculate hash: %v", err)
	}
	expected := "236d111d44009a6b081fa4bb590eb4dc116e0fc8c35a411079c3e29d952567f2a4fc558a16892bbfc2f35cdcde723cf926786321ff9b481430a300bab59c4330.dir"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
	hash, err = Dirhash(fs, "b", hasher2, hasher1, hasher1)
	if err != nil {
		t.Errorf("Could not calculate hash: %v", err)
	}
	expected = "e13645880205fcaf88a471c45767365d35ab8572e0da98a10898a531b9713545.dir"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
	hash, err = Dirhash(fs, "b", hasher1, hasher1, hasher2)
	if err != nil {
		t.Errorf("Could not calculate hash: %v", err)
	}
	expected = "cac6c905f3d00ec82c62dd73daae00a1b928a1134a979259d9629422c9244efa.dir"
	if hash != expected {
		t.Errorf("Hash was incorrect, got: %s, want: %s.", hash, expected)
	}
}

func createTestFileSystem() (billy.Filesystem, error) {
	fs := memfs.New()
	if err := createFile(fs, "/a", "a"); err != nil {
		return nil, err
	}
	if err := createFile(fs, "/b/b1", "b1"); err != nil {
		return nil, err
	}
	if err := createFile(fs, "/b/b2", "b2"); err != nil {
		return nil, err
	}
	if err := createFile(fs, "/b/b3", "b3"); err != nil {
		return nil, err
	}
	if err := createFile(fs, "/c", "c"); err != nil {
		return nil, err
	}
	if err := fs.MkdirAll("/d", 0777); err != nil {
		return nil, err
	}
	return fs, nil
}

func createFile(fs billy.Filesystem, path string, value string) error {
	f, err := fs.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()
	io.WriteString(f, value)
	return nil
}

package hash

import (
	"fmt"
	"hash"
	"io"
)

type HashingWriter struct {
	hasher      Hash
	h           hash.Hash
	multiwriter io.Writer
}

func NewHashingWriter(writer io.Writer, hash Hash) (*HashingWriter, error) {
	if !hash.Available() {
		return nil, fmt.Errorf("Hash %v is not available", hash)
	}
	h := hash.New()
	return &HashingWriter{
		hasher:      hash,
		h:           h,
		multiwriter: io.MultiWriter(writer, h),
	}, nil
}

func (hw *HashingWriter) GetHash() string {
	return hw.hasher.GetHash(hw.h)
}

func (hw *HashingWriter) Write(p []byte) (n int, err error) {
	return hw.multiwriter.Write(p)
}

package util

import "github.com/docker/docker/pkg/fileutils"

type FilterFunc func(path string) bool

func WithExcludes(excludes []string) (FilterFunc, error) {
	// do a test match to check the provided patterns for validity
	if _, err := fileutils.Matches(".", excludes); err != nil {
		return nil, err
	}
	return func(path string) bool {
		// we ignore error state now because we checked the patterns for
		// validity above
		excluded, _ := fileutils.Matches(path, excludes)
		return !excluded
	}, nil
}

package util

import (
	"archive/tar"
	"io"

	"github.com/go-git/go-billy/v5"
)

type TarReaderCloser struct {
	closer io.Closer
	tar.Reader
}

func NewTarReaderCloser(reader io.ReadCloser) *TarReaderCloser {
	return &TarReaderCloser{
		closer: reader,
		Reader: *tar.NewReader(reader),
	}
}

func (reader *TarReaderCloser) Close() error {
	return reader.closer.Close()
}

func CreateTarball(sourceFs billy.Filesystem, writer io.Writer) error {
	return CreateTarballWithFilter(sourceFs, writer, func(_ string) bool { return true })
}

func CreateTarballWithFilter(sourceFs billy.Filesystem, writer io.Writer, filter FilterFunc) error {
	tarWriter := tar.NewWriter(writer)
	err := addDirectoryToTar(sourceFs, tarWriter, "", filter)
	tarWriter.Close()
	return err
}

func addDirectoryToTar(sourceFs billy.Filesystem, writer *tar.Writer, path string, filter FilterFunc) error {
	fileInfos, err := sourceFs.ReadDir(path)
	if err != nil {
		return err
	}
	for _, fileInfo := range fileInfos {
		filePath := sourceFs.Join(path, fileInfo.Name())
		if fileInfo.IsDir() {
			err = addDirectoryToTar(sourceFs, writer, filePath, filter)
			if err != nil {
				return err
			}
		} else {
			// we only want to check files against the filter function to avoid the case
			// where a parent folder might get ignored but the file itself should get
			// included
			if !filter(filePath) {
				continue
			}
			err = writer.WriteHeader(&tar.Header{
				Name:    filePath,
				Size:    fileInfo.Size(),
				Mode:    int64(fileInfo.Mode()),
				ModTime: fileInfo.ModTime(),
			})
			if err != nil {
				return err
			}
			file, err := sourceFs.Open(filePath)
			if err != nil {
				return err
			}
			defer file.Close()
			_, err = io.Copy(writer, file)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

package util

import (
	"bytes"
	"io"
	"strings"
)

var lineEnds = "\r\n"

// ChannelWriter is a buffered writer that sends lines to a given channel
type ChannelWriter struct {
	channel chan<- string
	buffer  bytes.Buffer
}

// NewChannelWriter creates a new channel writer that writes to the given channel
func NewChannelWriter(channel chan<- string) *ChannelWriter {
	return &ChannelWriter{
		channel: channel,
	}
}

func (writer *ChannelWriter) Write(p []byte) (int, error) {
	n, err := writer.buffer.Write(p)
	if err != nil {
		return n, err
	}
	var line string
	for {
		line, err = writer.buffer.ReadString('\n')
		if err == nil {
			writer.channel <- strings.TrimRight(line, lineEnds)
		} else {
			break
		}
	}
	switch {
	case err == io.EOF:
		// write the incomplete string back to the buffer
		writer.buffer.WriteString(line)
	case err != nil:
		return n, err
	}
	return n, nil
}

// Close writes any remaining buffered string to the channel and resets the buffer
func (writer *ChannelWriter) Close() {
	remaining := writer.buffer.String()
	if remaining != "" {
		writer.channel <- strings.TrimRight(remaining, lineEnds)
	}
	writer.buffer.Reset()
}

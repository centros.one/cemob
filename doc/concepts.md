# Concepts

This document describes the most important core concepts relevant for understanding the operation of CeMoB when performing the training of models, building of model services and verifying of models.

## Models and model training

One of CeMoBs main goals is to be able to reproducibly create any arbitrary AI model. The process it uses to do this is outlined in the following diagram:

![Training Pipeline](img/training_pipeline.png)

Firstly CeMoB performs a checkout of the training data set and the model training code. It assumes that both are stored in separate repositories managed by a supported Version Control Software (e.g. [Git](https://git-scm.com/), [DVC](https://dvc.org/)). Then it prepares the training environment by taking a base Docker image, creating a container and copying all relevant data into it. After this it performs the actual training by running the training pipeline specified in the model training code. Currently only DVC pipelines are supported. The training parameters are provided as DVC pipeline params.

It is assumed that the training pipeline creates a least one file in the container filesystem that contains a serialized version of the trained model. There is no upper limit for the number of model files that are supported.

As a last step CeMoB copies those model files from the container and creates an updated Model Training Descriptor containing important meta data regarding the model.

### Model Training Descriptor

The [Model Training Descriptor (MTD)](../pkg/schemas/mtd.json) contains all data needed to execute the above process. Those are:

- A reference to the model training code repository
- A reference to the training data set repository
- A reference to the base Docker image
- The training parameters
- The list of model files that should be copied after training pipeline execution
- Arbitrary meta data that will not influence the training itself but will be part of the final Model Descriptor

The updated MTD contains the following additional information after the training process:

- A hash over the full model training code
- A hash over the full training data set

The MTD is the only explicit input needed when running model training with CeMoB. The referenced repositories need to be accessable by CeMoB for the training to succeed.

## Model service build

After performing model training the next step is to build the model service image. This is done to create a deployable artifact that can be used to execute the trained model in a Centros environment.

![Build Pipeline](img/build_pipeline.png)

During the build phase CeMoB takes the model files from the training step and creates a Docker image by copying them into a specified base image. CeMoB makes no assumption about the content of this base image. The base image needs to contain a Centros compatible service skeleton for the resulting service image to be Centros compatible.

Besides the service image itself CeMoB also creates a Model Descriptor that contains all necessary information for later reproducing and verifying the created service image.

### Model Runtime Descriptor

The [Model Runtime Descriptor (MRD)](../pkg/schemas/mrd.json) contains all data needed to execute the build process. Those are:

- A list of model files as named as in the MTD and where to copy them into the final service image
- A reference to the base Docker image

The list of model files can also include a hash for each model file to verify its content.

A full build requires the MTD as well as the MRD as input for CeMoB to be able to firstly create the model files and then copy them into the final service image.

### Model Descriptor

The [Model Descriptor (MD)](../pkg/schemas/md.json) is the final result of the build process and contains all data needed to reproduce and verify the model build. It contains:

- The full MTD including the hashes for the model training code and training data set
- The full MRD including the hashes for all model files
- A [build independent hash](image_hash.md) for the resulting service image

## Model verification

During model verification CeMoB takes the Model Descriptor as input and tries to recreate the model using the following process:

![Build Pipeline](img/verification_pipeline.png)

Firstly it takes the MTD from the MD and performs the same training process as described above with one difference. During the checkout of the model training code and training data set it checks the content of the files against the hashes in the MTD and aborts the process if any deviations are found.

If the training process completes successfully CeMoB then takes the MRD from the MD and compares the hashes of the created model files with the hashes from the MRD. If they are equal the build process is executed as described above.

In the last step CeMoB takes the image hash from the MD and compares it with the hash of the newly created service image. If they are equal the process completes without error.

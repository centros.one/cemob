# Defining a reproducible Docker image hash

Docker images as they are created by the Docker daemon pose some challenges with regards to reproducibility. Namely their content (and therefore their image IDs) contain timestamps that depend on the local time of the machine that is building the image.

To calculate a hash value that takes into account all of the elements of the image that are relevant to its observable behaviour while ignoring irrelevant metadata the following basic algorithm is used:

1. Strip the irrelevant and variable data from the image configuration
1. Calculate a hash value for each file system layer that ignores irrelevant metadata of files within
1. Replace the existing file system layer hashes with the new hashes
1. Serialize the configuration into a canonical representation
1. Calculate a hash over the serialized configuration

## Stripping irrelevant data

We define the following subset of the full image configuration that contains only stable elements we are interested in:

```golang
type ImageConfig struct {
	Config ConfigOptions `json:"config"`
	RootFs RootFs        `json:"rootfs"`
}

type ConfigOptions struct {
	User       string            `json:"User"`
	Env        []string          `json:"Env"`
	Cmd        []string          `json:"Cmd"`
	Entrypoint []string          `json:"Entrypoint"`
	WorkingDir string            `json:"WorkingDir"`
	Labels     map[string]string `json:"Labels,omitempty"`
}

type RootFs struct {
	Type    string   `json:"type"`
	DiffIds []string `json:"diff_ids"`
}
```

Namely those are environment variables, labels, entrypoint and command, the executing user and the working directory as well as the hashes for the file system layers. Additional fields could easily be added if necessary.

## Calculating a new hash for the file system layers

The algorithm `dirhash(h, dir)` receives the following input data:

1. a hash function `h(x)` (it is assumed that `h` always returns a string; this can be ensured by converting any numeric output to [hexadecimal notation](https://en.wikipedia.org/wiki/Hexadecimal))
1. a start directory `dir`

It then works recursively and consists of the following steps that are executed beginning in `dir`:

1. Sort the list of entries in the directory by name. The name is read as a sequence of bytes and no special handling for lexicographic sorting is done
1. For each entry `e`
    1. Calculate the hash `hash_sum`
        1. If it is a file read its content `c` and calculate `hash_sum = h(c)` (`c` is read as a sequence of bytes)
        1. If it is a [symbolic link](https://en.wikipedia.org/wiki/Symbolic_link) with the target `t` calculate `hash_sum = h(t) + ".link"` where `+` denotes string concatenation (`t` is read as a sequence of bytes)
        1. If it is a directory calculate `hash_sum = dirhash(h, e)`
    1. Append the string `hash_sum + " " + e.name + "\n"` to a buffer `b` where `+` again denotes string concatenation and `\n` represents the linebreak character
1. Calculate the hash `dir_hash = h(b)`
1. Return `dir_hash + ".dir"`

The value returned by this algorithm is sensitive to changes in the contents and names of files and directories contained within the directory hierarchy. It will also produce different results between a folder containing a symbolic link `a` that points to the path `b` and a file named `a` that contains the content `b`. The fact that it won't try to resolve any symbolic link to its actual target also means that broken symbolic links (e.g. symbolic links with an invalid target) will not produce any errors in the algorithm. Lastly it is also relatively easy to calculate this hash value by hand using common command line tools like `sha256sum`, `sort` and `printf`.

Another advantage is that this algorithm is agnostic with regards to the concrete hash function that is being used. So we can chose one appropriate for the level of security or tamper resistance we desire.

One should note that this is a conservative approach for checking the image file system for changes as this ensures that each file system layer is exactly equal between images. It is possible to construct images where certain file system layers differ from each other while the resulting image file system is exactly the same. This is because of the fact that the final image file system is created by overlaying all file system layers on top of each other. So creating a file system layer that contains a file that is being deleted in a subsequent layer (by using a [whiteout file](https://github.com/opencontainers/image-spec/blob/859973e32ccae7b7fc76b40b762c9fff6e912f9e/layer.md#whiteouts)) will have no effect on the final image file system while obviously changing the hash as it is described here. On the other hand by ensuring that all file system layers are exactly equal we also ensure that the final image file systems are equal.

## Finishing the configuration and final hashing

After calculating the new file system layer hashes we just need to replace each entry in the `ImageConfig.RootFs.DiffIds` array with its corresponding updated hash. After that we have to define a canonical form for this new configuration object. For this we use [canonical JSON](https://github.com/gibson042/canonicaljson-go), a specification for a subset of JSON that only has one valid representation per data object to be serialized.

After serialization we have an array of bytes that we can then use as input for an arbitrary hash function. Like with the hash function that is used to recalculate the file system layer hashes we can then use the hash function most appropriate for our security needs.

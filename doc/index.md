# CeMoB docs

Welcome to Centros Model Builder documentation.

Here you can find information regarding different topics regarding CeMoB usage and design.

## Overview

Here you can find an overview over topics covered by this documentation.

|  topic                              | Description                                                |
|:------------------------------------|:-----------------------------------------------------------|
| [Concepts](concepts.md)             | Important key concepts for using CeMoB.                    |
| [Docker image hash](image_hash.md)  | Description of a reproducible Docker image hash algorithm. |

# Borrowed from:
# https://gist.github.com/turtlemonvh/38bd3d73e61769767c35931d8c70ccb4

BINARY = cemob
GOARCH = amd64

VERSION ?= $(shell git describe --always --dirty --tags)
COMMIT ?= $(shell git rev-parse HEAD)

# Symlink into GOPATH
BUILD_DIR=dist

# Setup the -ldflags option for go build here, interpolate the variable values
LDFLAGS = -ldflags "-X gitlab.com/centros.one/cemob/pkg.version=${VERSION} -X gitlab.com/centros.one/cemob/pkg.versionHash=${COMMIT}"

# Build the project
all: clean linux darwin windows

linux:
	GOOS=linux GOARCH=${GOARCH} go build ${LDFLAGS} -o ${BUILD_DIR}/${BINARY}-linux-${GOARCH} . ; \

darwin:
	GOOS=darwin GOARCH=${GOARCH} go build ${LDFLAGS} -o ${BUILD_DIR}/${BINARY}-darwin-${GOARCH} . ; \

windows:
	GOOS=windows GOARCH=${GOARCH} go build ${LDFLAGS} -o ${BUILD_DIR}/${BINARY}-windows-${GOARCH}.exe . ; \

fmt:
	go fmt $$(go list ./... | grep -v /vendor/) ; \

test:
	go test -v ./...

clean:
	rm -fR "${BUILD_DIR}"
	mkdir -p "${BUILD_DIR}"

.PHONY: linux darwin windows fmt clean test

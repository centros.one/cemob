package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/centros.one/cemob/pkg"
)

func buildValidateCommand(c *pkg.CeMoB) *cobra.Command {
	validateCmd := &cobra.Command{
		Use:   "validate",
		Short: "Validate different descriptor files",
		Long:  `Validate model training descriptor or model runtime descriptor`,
	}

	validateCmd.AddCommand(buildValidateMtdCommand(c))
	validateCmd.AddCommand(buildValidateMrdCommand(c))
	validateCmd.AddCommand(buildValidateMdCommand(c))

	return validateCmd
}

func buildValidateMtdCommand(c *pkg.CeMoB) *cobra.Command {
	opts := pkg.ValidateMTDOpts{}

	validateCmd := &cobra.Command{
		Use:   "mtd",
		Short: "Validate model training descriptor syntax",
		RunE: func(cmd *cobra.Command, args []string) error {
			_, err := c.ValidateMTD(&opts)
			return err
		},
		Args:         cobra.ExactArgs(0),
		SilenceUsage: true,
	}

	validateCmd.Flags().StringVarP(&opts.MTD.SourcePath, "mtd", "f", "",
		"The model training descriptor to be validated")
	validateCmd.MarkFlagRequired("mtd")

	return validateCmd
}

func buildValidateMrdCommand(c *pkg.CeMoB) *cobra.Command {
	opts := pkg.ValidateMRDOpts{}

	validateCmd := &cobra.Command{
		Use:   "mrd",
		Short: "Validate model runtime descriptor syntax",
		RunE: func(cmd *cobra.Command, args []string) error {
			_, err := c.ValidateMRD(&opts)
			return err
		},
		Args:         cobra.ExactArgs(0),
		SilenceUsage: true,
	}

	validateCmd.Flags().StringVarP(&opts.MRD.SourcePath, "mrd", "f", "",
		"The model runtime descriptor to be validated")
	validateCmd.Flags().StringVar(&opts.MTD.SourcePath, "mtd", "",
		"The model training descriptor to be validating against")
	validateCmd.MarkFlagRequired("mrd")

	return validateCmd
}

func buildValidateMdCommand(c *pkg.CeMoB) *cobra.Command {
	opts := pkg.ValidateMDOpts{}

	validateCmd := &cobra.Command{
		Use:   "md",
		Short: "Validate model descriptor syntax",
		RunE: func(cmd *cobra.Command, args []string) error {
			_, err := c.ValidateMD(&opts)
			return err
		},
		Args:         cobra.ExactArgs(0),
		SilenceUsage: true,
	}

	validateCmd.Flags().StringVarP(&opts.MD.SourcePath, "md", "f", "",
		"The model descriptor to be validated")
	validateCmd.MarkFlagRequired("md")

	return validateCmd
}

package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/centros.one/cemob/config"
	"gitlab.com/centros.one/cemob/pkg"
)

// BuildRootCommand creates the root command
func BuildRootCommand() *cobra.Command {
	var configPath string
	c := pkg.New()

	rootCmd := &cobra.Command{
		Use:   "cemob",
		Short: "A orchestrator for reproducible model training",
		Long: `Centros Model Builder is an application that orchestrates
reproducible building of models and Centros model services.`,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			if configPath != "" {
				config.SetConfigSource(configPath)
			}
			if _, err := config.GetConfig(); err != nil {
				return err
			}
			return nil
		},
	}

	rootCmd.PersistentFlags().StringVarP(&configPath, "config", "c", "",
		"Path to the config file to be used")

	rootCmd.AddCommand(buildValidateCommand(&c))
	rootCmd.AddCommand(buildTrainCommand(&c))
	rootCmd.AddCommand(buildBuildCommand(&c))
	rootCmd.AddCommand(buildVerifyCommand(&c))
	rootCmd.AddCommand(buildVersionCommand(&c))

	return rootCmd
}

package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/centros.one/cemob/config"
	"gitlab.com/centros.one/cemob/pkg"
)

func buildTrainCommand(c *pkg.CeMoB) *cobra.Command {
	opts := pkg.TrainOpts{}

	trainCmd := &cobra.Command{
		Use:   "train",
		Short: "Train the model using the training descriptor",
		Long: `Train the model using the training descriptor.

During training the referenced code and data repositories will be cloned.
If this requires authentication you either need to provide username and
password or a specific private key that should be used.

If neither is specified and the repo URL is using SSH CeMoB will try to
connect to a running SSH agent on the socket specified in the SSH_AUTH_SOCK
env var. If this var is not set or a connection cannot be established an
error is thrown.

The model files specified in the training descriptor will be copied into
the output directory named as is.`,
		PreRunE: func(cmd *cobra.Command, args []string) error {
			config, _ := config.GetConfig()
			opts.FillFromConfig(config)
			return opts.Validate()
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			_, err := c.Train(&opts)
			return err
		},
		Args:         cobra.ExactArgs(0),
		SilenceUsage: true,
	}

	setTrainCommandFlags(trainCmd, &opts, true)

	return trainCmd
}

func setTrainCommandFlags(cmd *cobra.Command, opts *pkg.TrainOpts, includeMainFileFlag bool) {
	cmd.Flags().StringVar(&opts.CodeAuth.User, "code-user", "",
		"Username for cloning training code repository")
	cmd.Flags().StringVar(&opts.CodeAuth.Pass, "code-pass", "",
		"Password for cloning training code repository")
	cmd.Flags().StringVar(&opts.CodeAuth.KeyPath, "code-key-path", "",
		"Path to private key used for cloning training code repository")
	cmd.Flags().StringVar(&opts.CodeAuth.KeyPass, "code-key-pass", "",
		"Password for private key used for cloning training code repository")
	cmd.Flags().StringVar(&opts.DataAuth.User, "data-user", "",
		"Username for cloning training data repository")
	cmd.Flags().StringVar(&opts.DataAuth.Pass, "data-pass", "",
		"Password for cloning training data repository")
	cmd.Flags().StringVar(&opts.DataAuth.KeyPath, "data-key-path", "",
		"Path to private key used for cloning training data repository")
	cmd.Flags().StringVar(&opts.DataAuth.KeyPass, "data-key-pass", "",
		"Password for private key used for cloning training data repository")
	cmd.Flags().BoolVar(&opts.Copy, "copy-repos", false,
		"Copy code and data repos into the container instead of mounting them as volumes")
	cmd.Flags().StringVar(&opts.WorkingDir, "working-dir", "",
		"Working directory where temporary files will be written to (default: system temp dir)")
	cmd.Flags().StringVarP(&opts.OutputDir, "output-dir", "o", "",
		"Directory where output files should be written to")
	if includeMainFileFlag {
		cmd.Flags().StringVar(&opts.MTD.SourcePath, "mtd", "",
			"Specify the model training descriptor")
		cmd.MarkFlagRequired("mtd")
	}
}

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/centros.one/cemob/pkg"
)

func buildVersionCommand(c *pkg.CeMoB) *cobra.Command {
	versionCmd := &cobra.Command{
		Use:   "version",
		Short: "Print the version",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println(c.Version())
		},
		Args:         cobra.ExactArgs(0),
		SilenceUsage: true,
	}

	return versionCmd
}

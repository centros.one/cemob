package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/centros.one/cemob/config"
	"gitlab.com/centros.one/cemob/pkg"
)

func buildBuildCommand(c *pkg.CeMoB) *cobra.Command {
	buildCmd := &cobra.Command{
		Use:   "build",
		Short: "Build the service image from a model runtime descriptor",
		Long: `Build the service image from a model runtime descriptor and model files.

There are two ways of build the service image. The first performs a
model training beforehand. The second will take the results of a model
training that has already been performed and continues the build from
there.

During the build the specified base image will be extended by copying
all referenced model files to the appropriate file system location
within the new image.`,
	}

	buildCmd.AddCommand(buildBuildFullCommand(c))

	return buildCmd
}

func buildBuildFullCommand(c *pkg.CeMoB) *cobra.Command {
	opts := pkg.BuildFullOpts{}

	buildFullCmd := &cobra.Command{
		Use:   "full",
		Short: "Perform the model training and then build the service image",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			config, _ := config.GetConfig()
			opts.FillFromConfig(config)
			return opts.Validate()
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			_, err := c.BuildFull(&opts)
			return err
		},
		Args:         cobra.ExactArgs(0),
		SilenceUsage: true,
	}

	setTrainCommandFlags(buildFullCmd, &opts.TrainOpts, true)
	setBuildCommandFlags(buildFullCmd, &opts, true)

	return buildFullCmd
}

func setBuildCommandFlags(cmd *cobra.Command, opts *pkg.BuildFullOpts, includeMainFileFlag bool) {
	cmd.Flags().StringSliceVar(&opts.Tags, "tag", nil,
		"A tag for the image that is being built")
	cmd.Flags().BoolVar(&opts.PushTags, "push-tags", false,
		"Push all provided tags to their remote registries")
	if includeMainFileFlag {
		cmd.Flags().StringVar(&opts.MRD.SourcePath, "mrd", "",
			"The model runtime descriptor to be built")
		cmd.MarkFlagRequired("mrd")
	}
}

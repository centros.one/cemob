package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/centros.one/cemob/config"
	"gitlab.com/centros.one/cemob/pkg"
)

func buildVerifyCommand(c *pkg.CeMoB) *cobra.Command {
	opts := pkg.VerifyOpts{}

	verifyCmd := &cobra.Command{
		Use:   "verify",
		Short: "Verify a given model descriptor",
		Long: `Perform verification for a given model descriptor

This will first train perform model training according to the MTD
within the MD. During this all resulting files will be checked
against the hashes from the MD.

Then the service image will be built according to the MRD within
the MD. For this service image a model descriptor will be written
and checked against the input model descriptor. If the resulting
image hash is different an error will be thrown.`,
		PreRunE: func(cmd *cobra.Command, args []string) error {
			config, _ := config.GetConfig()
			opts.FillFromConfig(config)
			return opts.Validate()
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			_, err := c.Verify(&opts)
			return err
		},
		Args:         cobra.ExactArgs(0),
		SilenceUsage: true,
	}

	setTrainCommandFlags(verifyCmd, &opts.BuildFullOpts.TrainOpts, false)
	setBuildCommandFlags(verifyCmd, &opts.BuildFullOpts, false)
	verifyCmd.Flags().StringVarP(&opts.MD.SourcePath, "md", "f", "",
		"The model descriptor to be verified")
	verifyCmd.MarkFlagRequired("md")

	return verifyCmd
}
